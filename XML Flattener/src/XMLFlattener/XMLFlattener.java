/*
 * 
 * Kevin Young
 * 109561787
 * Homework 3
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Scanner;
import java.util.Stack;

/**
 * Reads an XML file and prints the full path to each data element 
 * along with the data itself. The input filename must be received from the 
 * keyboard by prompting the user. If any syntax errors are detected in the
 * XML file (ie, an unclosed opening tag), an error message is printed, and
 * the program quits. The output format is one data element per line, with the 
 * path and a colon preceding the data element
 */
public class XMLFlattener {	
	
	static Stack<String> stack = new Stack<String>(); 		// To keep track of the path to each data element
	static Stack<String> tempStack = new Stack<String>();   // Temporary stack to assist with stack operations
	static Stack<String> dataStack = new Stack<String>();   // To hold all the tags and data in the XML file
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		Reader file = null;
		boolean fileFound;
		
		/* Prompt user for file name */
		System.out.print("Enter the name of the XML file: ");	 
		do {
			try {
				file = new FileReader(input.next());
				fileFound = true;
			} catch (FileNotFoundException e) {
				fileFound = false;
				System.out.print("File not found.\n");
				System.out.print("Enter the name of the XML file: ");	 
			} 
		}
		while (!fileFound);
		
		System.out.println();
		String tag = ""; 			  // The name of the tag being looked at
		boolean tagFound = false;     // Whether or not a tag has been found
		boolean notClosedTag = false; // Whether or not the current tag is a closing tag
		StringBuilder sb = new StringBuilder(); 
		String line; 	              // The line of the text file 
		String everything = "";       // The full contents of the file will be put into this String
		int indexOfDataEnd = 0;       // The index of everything where the data name ends 
		int indexOfCloseCar = 0;      // The index of everything where the closing tag begins
		int closeTagEnd = 0; 	      // The index of everything where the closing tag ends
		int openTagEnd = 0; 	      // The index of everything where the opening tag ends	
		String closeTagStr = "";
		String openTagStr = "";
		
		/* Get the contents of the XML file into one String */
	    BufferedReader br = new BufferedReader(file);
	    try {
			while((line = br.readLine()) != null) {
			    	sb.append(line.trim());	
			}
		} catch (IOException e1) {
			System.out.print("Error with the BufferedReader.\n Exiting program..");
			System.exit(0);
		}
	    everything = sb.toString().trim(); 
	    
	    /* This catches an empty XML file when the XML file is in UTF-8, UTF-16BE, UTF-16LE, and ANSI encoding, respectively.
            Note this may not work on every IDE. */
	    if (everything.equals("﻿") || everything.equals("��") ||  everything.equals("��") || everything.equals("")) {  
		   		System.out.println("This XML file is empty");
		   		System.out.print("Exiting..");
		   		System.exit(0);
	    }
	    
	    /* Iterate through the text file character by character */
	    for (int i = 0; i < everything.length(); i++) {
	       	if (everything.charAt(i) == '<') {
	       		openTagEnd = everything.indexOf('>', i);
	       		
	       		/* Checking for singular tag. Delete singular tag if one is found */
	       		if (everything.charAt(openTagEnd-1) == '/') {
	       			everything = everything.replaceFirst(everything.substring(i, openTagEnd),"");
	       			continue; 
	       		}			
	       		
	       		/* Checking for the start of a closing tag */
	       		if (everything.charAt(i+1) == '/') { 
                            for (int k = (i+1); everything.charAt(k-1) != '>'; k++) { 		        			
                                if (everything.charAt(k) == '>') {
                                        closeTagEnd = k;
                                }
                            }
                            closeTagStr = everything.substring(i+2, closeTagEnd); // The name of the closing tag	        			

                            /* Checking if there is an unmatched closing tag (ex. </meat> exists while there is no <meat> */
                            if (stack.empty()) {
                                    System.out.println("\nInvalid XML. Extra closing tag \"<" + closeTagStr + ">\"");
                                    System.out.print("Exiting program..");
                                    System.exit(0);
                            }

                            /* Checking if a closing tag closes the wrong tag (ex. </meat> closes <items> */
                            tempStack.push(stack.pop()); // removing the "/" from the stack so the top tag name will be printed
                            if (!(checkTag(closeTagStr))) {	        				
                                    System.out.println("\nInvalid XML. \"</" + closeTagStr + ">\" closes \"</" + stack.peek() + ">\"");
                                    System.out.print("\nExiting program..");
                                    System.exit(0);
                            }
                            else {
                                    stack.push(tempStack.pop());
                            }

                            /* Putting the elements back into the original stack */
                            while (!tempStack.empty()) { 
                                    stack.push(tempStack.pop());
                            }

                            /* Beginning to get data elements */
                            for (int j = i; j > 0; j--) {
                                    if (everything.charAt(j) == '>') {
                                            indexOfDataEnd = j;
                                            break;
                                    }
                            }
                            tag = everything.substring(indexOfDataEnd+1, i).trim(); // The name of the data element

                            /* Delete any end-of-tag so it doesn't get printed */
                            if (tag.equals("")) {
                                    if (!stack.empty()) { 
                                            stack.pop();
                                            stack.pop();
                                    }
                                    continue;
                            }

                            /* If a tag is found which doesn't match the top of the stack, then push this tag. 
                             * Otherwise, pop this tag and print the stack */
                            if (tagFound == true) {
                                    if (stack.empty() || !checkTag(tag)) {
                                            stack.pop();
                                            stack.push(":"); 	// we push a colon for formatting purposes when we print the stack contents
                                            pushDataStack(tag);		        			
                                            stack.push(tag);
                                    }
                                    stack.pop(); // pop the data 
                                    stack.pop(); // pop the colon
                                    printStack();	        					
                                    System.out.println(": " + tag);
                                    stack.pop(); // pop the tag
                                    notClosedTag = true;
                            }
	       		}
	       		
	       		/* Handling an opening tag.  If an opening tag is found which doesn't match the top of the stack,
	       		 * then push this tag. */
	        	else {
	        		openTagStr = everything.substring(i+1, openTagEnd).trim(); // the name of the opening tag	        		
	        		if (!(checkTag(openTagStr))) {
	        			stack.push(openTagStr); // push the open tag name onto the stack if it doesn't match the top of the stack
	        			stack.push("/");		// we push "/" so that when we print the elements, the tags are separated with a "/"
	        		}
	        		tagFound = true; 			// indicates a tag was found
	        		notClosedTag = true;		// indicates this found tag is not a closed tag
	        	}
	        	try {
	        		/* Looking at data which is between two open tags (Ex. <vegetables> [data] <potatoes>) ?????*/
	        		if (everything.charAt(openTagEnd+1) != '<' && everything.charAt(everything.indexOf('<',openTagEnd) + 1) != '/') {
	        			for(int j = openTagEnd; everything.charAt(j-1) != '<'; j++) {
	        				if (everything.charAt(j) == '<') {
	        					indexOfCloseCar = j;
	        				}
	        			}
	        			tag = everything.substring(openTagEnd+1, indexOfCloseCar).trim();	// name of the data
	        			notClosedTag = true;
	        			if (tag.equals("")) { // indicates the end of a tag, so delete this tag so it doesn't get printed
	        				if (everything.charAt(everything.indexOf('<', openTagEnd)+1) != '/') // ignore the pop() 
	        																				     // if there is a space between two opening tags	        																						
	        					continue;
	        				stack.pop(); // gets rid of the forward slash
	        				stack.pop(); // gets rid of the tag name
	        				continue;
	        			}
	        			
	        			/* If we're at data between two tags, pop the "/" at the top of the stack and push a ":" so that the format will be Tag/Tag: data*/
	        			if (tagFound == true ) {
	        				if (notClosedTag == true) {
	        					stack.pop();
	        					stack.push(":");
	        				}
	        				if (stack.empty() || !(checkTag(tag))) { 	        					
	        					pushDataStack(tag);
	        					stack.push(" " + tag);
	        				}	        					
	        				printStack();	  
	        				System.out.println();
	        				stack.pop(); 		// Pop the data that was just pushed in
	        				stack.pop(); 		// Pop the colon
	        				stack.push("/");
	        			}
	        		} 
	        	} catch (StringIndexOutOfBoundsException e) { // Reached end of file  
	        			break;
	        	  }
	        }
	    }	 
	    /* Checking to see if there's an unclosed tag */
	    if (!stack.empty()) {
	       	stack.pop(); 		// get rid of the "/" so we can print the tag name
			System.out.print("\nInvalid XML. Unclosed tag \"<" + stack.pop() + ">\"\n");
			System.out.print("Exiting program..");
			System.exit(0);
	    }
	   
	    
/******************** THE FOLLOWING IS PART 2 OF THE EXTRA CREDIT ********************************** 
*                                         														   *	
*                                  SPECIFICATIONS: 												   *
* 																								   *
*    "Provide an option to retrieve data from the path. For example, given the XML file,		   *
*     "tag1/tag2: First data																	   *
*      tag1: Second data"																		   *
*    If the user types tag1/tag2, the program should output First data,						   *
*    and if the user types tag1, the program should output Second data."					   *
*          																						   *
****************************************************************************************************/
	    
	    /* Present a menu to the user, keep asking until "n" or "N" is entered. When this is entered, terminate
	     * the program   */
	    boolean searchXML = false;
	    do {
	    	System.out.print("\nRetrieve data from a path? (Y/N): ");
	  	    String response = input.next();
	  	    response = response.toUpperCase();
	    	switch (response) {
	    		case "Y": System.out.print("Enter the path (ex. [tag1]/[tag2]): ");
	    				  String element = input.next();
	    				  searchDataStack(element);
	    			  	  searchXML = true;
	    			  	  break;
	    		case "N": System.out.print("Exiting program..");
	    		    	  System.exit(0);
	    		    	  break;
	    		default:  System.out.println("Invalid input. Try again");
	    		          searchXML = true;
	    			      break;
	    	}
	    }
	    while (searchXML);
	}

	/**
	 *  Pops the elements of the stack off one by one, then prints each element and pushes it onto 
	 *  a second stack. When the end is reached, the elements are popped off the second stack one 
	 *  by one and pushed back onto the original stack.
	 */
	public static void printStack() {		
		while (!stack.empty()) {
			tempStack.push(stack.pop());
		}
		while (!tempStack.empty()) {
			System.out.print(tempStack.peek());
			stack.push(tempStack.pop());
		}		
	}	
	
	/**
	 * Checks to see if the tag in the XML file matches the top of the stack (ignoring case).
	 * When the tag does not match the top of the stack, then the tag name is extracted 
	 * and pushed onto the stack.
	 * <dt><b>Precondition:</b><dd>
	 * This stack is not empty
	 * @param tag the name of the tag in the XML file 
	 * @return whether or not this tag matches the top of the stack
	 */
	public static boolean checkTag(String tag) {
		
		if (stack.empty())
			return false;
		else {
			if (tag.equalsIgnoreCase((String)stack.peek()))
				return true;
			else
				return false;
		}
	}
	
	/** Pushes a String onto a stack. This stack essentially holds all tags and
	 *  data of the XML file in the order that they are read.
	 *  @param element the String that will be pushed onto dataStack
	 */
	public static void pushDataStack(String element) {
		/* The purpose of this stack is to be able to get the data of a path. */
		dataStack.push(element);
		tempStack = (Stack<String>)stack.clone();
			while (!tempStack.empty()) {
				dataStack.push(tempStack.pop());
			}					
	}
	
	/**
	 * Takes a String which represents the path name inside the XML file, 
	 * and returns the data associated with this path name.
	 * @param path the path name inside the XML file
	 */
	public static void searchDataStack(String path) {
		
	/* This path name is split into an array, and when the first
	 * path tag (ex. given, "tag1/tag2", "tag1" would be the first path tag)
	 * matches the top of the stack, then we search the rest of the path tags
	 * to see if they match. If we match the full path, and the next element in
	 * the stack is a colon (representing that the next element after this colon
	 * is data and not a tag) then we print this data because we know it's a match
	 */
		tempStack = (Stack<String>)dataStack.clone();
		boolean doneSearching = false;
		path = path.replaceAll("/", " / ");
		String[] str = path.split(" ");
		do {
			while (!tempStack.empty()) {
				String data = tempStack.pop();
				if (str[0].equalsIgnoreCase(dataStack.peek())) {
					for (int i = 0; i < str.length; i++) {
						if (str[i].equalsIgnoreCase(data) && i == str.length-1 && tempStack.peek().equals(":")) {
							tempStack.pop();
							System.out.println("Data: " + tempStack.peek());
							doneSearching = true;
						}
						if (str[i].equalsIgnoreCase(data)) {
							data = tempStack.pop();
							continue;
						}
					}
				}
			}			
			if (!doneSearching) {
				System.out.println("Path not found");
				doneSearching = true;
			}
		}
		while (!doneSearching);		
	}
}