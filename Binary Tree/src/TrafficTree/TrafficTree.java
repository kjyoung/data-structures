/*
 * 
 * Kevin Young
 * 109561787
 * Homework 5
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Represents a tree of the roads in an area. Two int constants NORTH and SOUTH are stored.
 * This class must store the root of the tree (TrafficNode reference)
 * @author Kevin
 *
 */
public class TrafficTree implements Serializable {
	
	private static final int NORTH = 1; 
	private static final int SOUTH = 3;
	TrafficNode root;
	
	/**
	 * Constructs an empty TrafficTree
	 */
	public TrafficTree() {
		root = null;
	}	
	
	/**
	 * Finds the node representing the specified road, if it exists, and adds the speed to the appropriate
	 * list in that node specified by the direction parameter, which should be one of NORTH or SOUTH. If
	 * the node representing the specified road does not exist, this method creates it, adds the speed to it,
	 * and adds it to the tree.
	 * @param road the specified road which is of the node this method searches for
	 * @param speed the speed to add to the list in the node specified by direction
	 * @param direction NORTH or SOUTH
	 */
	 public void addSpeed(String road, double speed, int direction)  {
		 if (root == null) {
			 root = new TrafficNode(road);
			 root.setSpeed(speed, direction); 
			 return;
		 }
		 TrafficNode node = root;
		 TrafficNode parent;
		 while (node != null) {
			 if (road.compareToIgnoreCase(node.getData()) < 1) { // This tree is sorted alphabetically
				 if (node.getData().equalsIgnoreCase(road)) {
					 node.setSpeed(speed, direction);
					 return;
				 }
				 node = node.getLeft();
			 }
			 else {
				 if (node.getData().equalsIgnoreCase(road)) {
					 node.setSpeed(speed, direction);
					 return;
				 }
				 node = node.getRight();
			 }
		 }
		 /* If we reach this point, the node with the specified data was not found.
		  * So a new node must be created */
		 node = root;
		 while (node != null) {
			 parent = node;
			 if (road.compareToIgnoreCase(node.getData()) < 1) { // This tree is sorted alphabetically
				 node = node.getLeft();												           
				 if (node == null) {
					 node = new TrafficNode(road);
					 node.setSpeed(speed, direction);
					 parent.setLeft(node);
					 return;
				 }						 
			 }
			 else {							
				 node = node.getRight();
				 if (node == null) {
					 node = new TrafficNode(road);
					 node.setSpeed(speed, direction);
					 parent.setRight(node);
					 return;
				 }
			 }
		 }
	 }

	 /**
	  * Opens the specified text file and adds all its data to this tree. The data format will be one speed 
	  * per line, with the road name, speed, and direction (either N or S) separated by commas. The method
	  * closes the file before returning from this method.
	  * @param filename text file to open and take data from
	  */
	 public void addSpeeds(String filename) throws IOException {
		 BufferedReader br = null;
		 String line = "";
		 br = new BufferedReader(new FileReader(filename));
		 String[] fileContents = new String[3];
		 /* Get the contents of the text file into one String */		    
		 while((line = br.readLine()) != null) {
			 fileContents = line.split(",");
			 int direction = fileContents[2].equals("N") ? NORTH : SOUTH;
			 addSpeed(fileContents[0], Double.parseDouble(fileContents[1]), direction);				    										  
		 }
		 br.close();
	 }
	 
	 /**
	  * Searches the tree for the given road and returns the average speed in the indicated direction, which
	  * will be one of NORTH or SOUTH. If the road was not found, the method returns -1. If the road was found, 
	  * but there were no cars traveling in the specified direction, the method returns -2.
	  * @param road the road to search the tree for
	  * @param direction NORTH or SOUTH
	  * <dt><b>Precondition:</b><dd>
	  *  The road exists, and if it does, then there are cars traveling in the specified direction
	  * @return the average speed in the indicated direction
	  */
	 public double findSpeed(String road, int direction) {
		 TrafficNode node = root;
		 while (node != null) {
			 if (node.getData().equalsIgnoreCase(road)) {
				 double avgSpeed = (direction == NORTH ? node.getAverageNorthSpeed() : node.getAverageSouthSpeed()); 
				 if (avgSpeed == -2)
					 return -2;
				 else
					 return avgSpeed;
			 }
			 else {
				 if (road.compareToIgnoreCase(node.getData()) < 1) // This tree is sorted alphabetically
					node = node.getLeft();
				 else
					node = node.getRight();
			 }
		 }
		 return -1;  // returns -2 if the road was found
					 // but there were no cars traveling in specified direction
		 			 // and returns -1 if the road was not found
	 }
	 
	 /**
	  * Prints the maximum depth of this tree (a tree of one single node has depth 0)
	  * @return the max depth of this tree
	  */
	 public int depth() {
		 if (isEmpty())
			 return -1;
		 else
			 return root.depth(0);		 
	 }
	 
	 /**
	  * Uses an inorder traversal to print the name of each road in the tree along with the average of the northbound
	  * and southbound speeds (separately) stored in each node
	  * @throws IllegalArgumentException if this tree is empty
	  */
	 public void printInorder() {
		 if (isEmpty())
			 throw new IllegalArgumentException("Unable to print contents of an empty tree.");
		 root.printInorder();
	 }
	 
	 /**
	  * Prints the data values of the given node
	  * @param node the node to print data of
	  */
	 public void printData(TrafficNode node) {
		 double avgNorthSpeed = node.getAverageNorthSpeed();
		 double avgSouthSpeed = node.getAverageSouthSpeed();
		 
		 System.out.println(node.getData() + ": " + "Avg speed North = " + (avgNorthSpeed == -2 ? "(none)" : avgNorthSpeed) + ". " +
				 "Avg speed South = " + (avgSouthSpeed == -2 ? "(none)" : avgSouthSpeed) + ".");
	 }
	 
	 /**
	  * Uses a preorder traversal to print the name of each road in the tree along with the average of the northbound
	  * and southbound speeds (separately) stored in each node.
	  * @throws IllegalArgumentException if this tree is empty
	  */
	 public void printPreorder() {
		 if (isEmpty())
			 throw new IllegalArgumentException("Unable to print contents of an empty tree.");
		root.printPreorder();
	 }
	 
	 /**
	  * Opens a FileOutputStream and an ObjectOutputStream, and uses the ObjectOutputStream to write the tree to the 
	  * specified binary file, then the method closes the two output streams.
	  * @param filename the binary file to write the tree's content to
	  * @throws IOException 
	  */
	 public void serialize(String filename) {
		 ObjectOutputStream outStream = null;
		 try {
			 FileOutputStream file = new FileOutputStream(filename);
			 outStream = new ObjectOutputStream(file);
			 outStream.writeObject(this); // Writes this object to �"filename".obj�
			 outStream.close();
			 file.close();
		 }
		 catch (IOException e) {
			 System.out.println("Unable to write this object to the file \"" + filename + "\"");
		 }
	 }
	 
	 /**
	  * Opens a FileInputStream and an ObjectInputStream, and uses the ObjectInputStream to read a tree from the
	  * specified binary file, then closes the two output streams and returns the tree. Returns null 
	  * if the file was not found, or did not contain a valid tree object.
	  * @param filename the binary file where the tree's data will be read from
	  * <dt><b>Precondition:</b><dd>
	  *  The file was found, and if it does, the file contains a valid tree object
	  * @return the tree in the ObjectInputStream
	  * @throws IOException 
	  * @throws ClassNotFoundException 
	  */
	 public static TrafficTree deserialize(String filename) throws ClassNotFoundException, IOException {
		 TrafficTree trafficTree;
		 try {
		      FileInputStream file = new FileInputStream(filename);
		      ObjectInputStream inStream  = new ObjectInputStream(file);
		      trafficTree = (TrafficTree) inStream.readObject(); 
		      inStream.close();
		      file.close();
		 } catch (FileNotFoundException e) {
			 return null;
		 } catch (ClassNotFoundException e) {
			 return null;
		 } catch (ClassCastException e) { 
			 return null;
		 } catch (IOException e) {
			 return null;
		 }
		 return trafficTree;
	 }
	 
	 /** 
	  * For use with GUI 	   
	  * @return a StringBuilder which represents the tree traversed by inorder
	  */
	 public StringBuilder printInorderGUI() {
		 if (isEmpty())
			 throw new IllegalArgumentException("Unable to print contents of an empty tree.");
		 StringBuilder inorderSB = new StringBuilder();
		 return root.printInorderGUI(inorderSB);
	 }
	 
	 /** 
	  * For use with GUI
	  * @return a StringBuilder which represents the tree traversed by preorder
	  */
	 public StringBuilder printPreorderGUI() {
		 if (isEmpty())
			 throw new IllegalArgumentException("Unable to print contents of an empty tree.");
		 StringBuilder preorderSB = new StringBuilder();
		 return root.printPreorderGUI(preorderSB);		 
	 }	 
	 
	 /**
	  * Returns if this tree is empty or not
	  * @return true if this tree is empty, otherwise return false
	  */
	 public boolean isEmpty() {
		 return root == null;
	 }	 
	 
	 /**
	  * Gets the root of the tree
	  * @return the root of the tree
	  */
	 public TrafficNode getRoot() {
		 return root;
	 }
	 
}