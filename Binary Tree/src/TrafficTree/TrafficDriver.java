/*
 * 
 * Kevin Young
 * 109561787
 * Homework 5
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.io.IOException;
import java.util.Scanner;

/**
 * The driver class which provides a main method which checks for a binary file
 * named tree.obj in the current working directory, and loads a tree from that file
 * if it exists. Then, a menu is displayed which gives options to perform various
 * operations on the TrafficTree class.
 * @author Kevin
 */
 public class TrafficDriver { 
	
	private static boolean runAgain = true;
	private static Scanner input = new Scanner(System.in);
	private static TrafficTree trafficTree = new TrafficTree();
	private static String filename = "tree.obj";
	
	public static void main(String[] args) {
		loadTreeFromFile();
		while (runAgain)
			printMenu();
	}
	
	/** Gets a tree object file and sets it as the tree, if the tree exists. */
	public static void loadTreeFromFile() {
		TrafficTree tree;
		try {
			tree = TrafficTree.deserialize(filename);
			if (tree != null) {
				System.out.println("Loaded tree from \"" + filename + "\"\n");
				trafficTree = tree;
			} else {
				System.out.println("\"" + filename + "\" not found or not a tree type. Using new tree.\n");		
			} 
		} catch (ClassNotFoundException e) {
			System.out.println("\"" + filename + "\" not a tree type. Using a new tree\n");
		} catch (IOException e) {
			System.out.println("\"" + filename + "\" not found\n");
		}		
	}	
	
	/** Prints a menu which the user will choose from to perform an operation */
	public static void printMenu() {		
		System.out.println("A) Add a speed\n" +
				"F) Load a text file\n" +
				"R) Find a road\n" +
				"I) Print all roads (inorder)\n" +
				"P) Print all roads (preorder)\n" +
				"D) Get depth\n" +
				"Q) Quit and save to \"tree.obj\"\n");
		System.out.print("Enter a selection: ");
		String selection = input.next().toUpperCase();
		
		switch (selection) {
			case "A": System.out.println();
					  addASpeed();
					  System.out.println();
					  break;
			case "I": System.out.println();
					  printInorder();
					  System.out.println();
					  break;
			case "P": System.out.println();
					  printPreorder();
					  System.out.println();
					  break;
			case "D": System.out.println();
					  getDepth();
					  System.out.println();
					  break;
			case "R": System.out.println();
					  findSpeed();
					  System.out.println();
					  break;
			case "Q": System.out.println();
					  quitAndSaveObj();
					  System.out.println();
					  break;
			case "F": System.out.println();
					  readFromTextFile();
					  System.out.println();
					  break;
		}		
	}
	
	/** Add a speed to the tree */
	public static void addASpeed() {
		System.out.print("Enter the road name: ");
		input.nextLine();
		String roadName = input.nextLine();
		System.out.print("Enter the speed: ");
		double speed = input.nextDouble();
		System.out.print("Enter the direction (N or S): ");
		String directionStr = input.next().toUpperCase();
		int direction = directionStr.equals("N") ? 1 : 3;
		trafficTree.addSpeed(roadName, speed, direction);		
		System.out.println("\nAdded speed " + speed + ", direction " + directionStr + " to " + roadName);
	}
	
	/** Print this tree using inorder traversal */
	public static void printInorder() {
		try {
			trafficTree.printInorder();
		}
		catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
	}
	
	/** Print this tree using preorder traversal */
	public static void printPreorder() {
		try {
			trafficTree.printPreorder();
		}
		catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
	}
	
	/** Get the depth of this tree */
	public static void getDepth() {
		System.out.println("Count depth: " + trafficTree.getRoot().countAtDepth(2));
		
		if (trafficTree.isEmpty())
			System.out.println("Unable to get depth of an empty tree.");
		else
			System.out.println("The maximum depth of the tree is " + trafficTree.depth());
	}
	
	/** Finds the given road and returns its average speed in the specified direction */
	public static void findSpeed() {
		if (trafficTree.isEmpty()) {
			System.out.println("Unable to retrieve a road from an empty tree.");
			return;
		}		
		System.out.print("Enter the road name: ");
		input.nextLine();
		String roadName = input.nextLine();
		System.out.print("Enter the direction (N or S): ");
		int direction = (input.next().toUpperCase().equals("N")) ? 1 : 3;
		double speed = trafficTree.findSpeed(roadName, direction);
		if (speed == -1)
			System.out.println("\nUnable to find this road.");
		else if (speed == -2)
			System.out.println("\nThere are no cars traveling " + (direction == 1 ? "northbound" : "southbound")
					+ " on " + roadName);
		else 
			System.out.println("\nThe average speed of " + (direction == 1 ? "northbound" : "southbound") 
					+ " cars on " + roadName + " is " + speed);
	}
	
	/** Quits the program and saves the current tree to an .obj file */
	public static void quitAndSaveObj() {
		trafficTree.serialize(filename);
		System.out.println("Saved tree to \"" + filename + "\"");
		System.exit(0);
	}
	
	/** Reads from a text file and extracts the data to add to the current tree */
	public static void readFromTextFile() {
		System.out.print("Enter the text file name: ");
		input.nextLine();
		String textFileName = input.nextLine();
		try {
			trafficTree.addSpeeds(textFileName);
			System.out.println("\nLoaded data from \"" + textFileName + "\" into the tree");
		} catch (IOException e) {
			System.out.println("\nFile \"" + textFileName + "\" cannot be found.");
		}			
	}
}