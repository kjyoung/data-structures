/*
 * 
 * Kevin Young
 * 109561787
 * Homework 5
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Objects of this class are the nodes of the tree. The name of the road is stored as a String,
 * and its 2 children (references of type TrafficNode) are referenced as "rightChild" and "leftChild"
 * The class also contains Lists of speeds (references of type List<Double> which point to instances of a class that
 * implements the List interface)
 * @author Kevin
 *
 */
public class TrafficNode implements Serializable {
	
	private TrafficNode rightChild;
	private TrafficNode leftChild;
	private String roadName;
	private ArrayList<Double> northSpeeds = new ArrayList<Double>();
	private ArrayList<Double> southSpeeds = new ArrayList<Double>();
	
	/**
	 * Construct an empty node
	 */
	public TrafficNode() {}
	
	public int countAtDepth(int d) {
		int count = 1; int rightCount = 1; int leftCount = 1;
		if (leftChild!=null)
			leftCount += leftChild.countAtDepth(d-1);
		if (rightChild!=null)
			rightCount+= rightChild.countAtDepth(d-1);
		if (d <= 0)
			return leftCount + rightCount;
		return 1;
}
	
	/**
	 * Construct a node with the given road name 
	 * @param roadName the road name to assign this new node
	 */
	public TrafficNode(String roadName) {
		this.roadName = roadName;
	}

	/**
	 * Get the depth of the tree
	 * @return the depth of the tree
	 */
	public int depth(int count) {
		int leftDepth = count;
		int rightDepth = count;

		if (leftChild != null)
			leftDepth = leftChild.depth(count + 1);
		if (rightChild != null)
			rightDepth = rightChild.depth(count + 1);

		return (leftDepth > rightDepth ? leftDepth : rightDepth);
	}	 

	/**
	 * Prints the data values of this node
	 */
	public void printData() {
		double avgNorthSpeed = getAverageNorthSpeed();
		double avgSouthSpeed = getAverageSouthSpeed();

		System.out.println(roadName + ": " + "Avg speed North = " + (avgNorthSpeed == -2 ? "(none)" : avgNorthSpeed) + ". " +
				"Avg speed South = " + (avgSouthSpeed == -2 ? "(none)" : avgSouthSpeed) + ".");
	}

	/**
	 * Uses a preorder traversal to print the name of each road in the tree along with the average of the northbound
	 * and southbound speeds (separately) stored in each node.
	 */
	public void printPreorder() {
		printData();
		if (leftChild != null)
			leftChild.printPreorder();
		if (rightChild != null)
			rightChild.printPreorder();
	}

	/**
	 * Uses an inorder traversal to print the name of each road in the tree along with the average of the northbound
	 * and southbound speeds (separately) stored in each node.
	 */
	public void printInorder() {
		if (leftChild != null)
			leftChild.printInorder();
		printData();
		if (rightChild != null)
			rightChild.printInorder();
	}

	/**
	 * To use for the GUI
	 * @param inorderSB the StringBuilder which will hold the inorder traversal of this tree
	 * @return the data values of this tree in inorder traversal
	 */
	public StringBuilder printInorderGUI(StringBuilder inorderSB) {
		if (leftChild != null) 
			leftChild.printInorderGUI(inorderSB);
		inorderSB.append(printDataGUI());
		if (rightChild != null)
			rightChild.printInorderGUI(inorderSB);

		return inorderSB;
	}		
	
	/**
	 * To use for the GUI
	 * @param preorderSB the StringBuilder which will hold the preorder traversal of this tree
	 * @return the data values of this tree in preorder traversal
	 */
	public StringBuilder printPreorderGUI(StringBuilder preorderSB) {
		preorderSB.append(printDataGUI());
		if(leftChild != null)
			leftChild.printPreorderGUI(preorderSB);
		if (rightChild != null)
			rightChild.printPreorderGUI(preorderSB);
		
		return preorderSB;
	}

	/**
	 * Returns the data values of the current node
	 */
	public String printDataGUI() {
		double avgNorthSpeed = getAverageNorthSpeed();
		double avgSouthSpeed = getAverageSouthSpeed();

		return (roadName + ": " + "Avg speed North = " + (avgNorthSpeed == -2 ? "(none)" : avgNorthSpeed) + ". " +
				"Avg speed South = " + (avgSouthSpeed == -2 ? "(none)" : avgSouthSpeed) + ".\n");
	}

	/**
	 * Gets the road name of this node
	 * @return the road name of this node
	 */
	public String getData() {
		return roadName;
	}
	
	/**
	 * Sets the road name of this node
	 * @param roadName the new road name of this node
	 */
	public void setData(String roadName) {
		this.roadName = roadName;
	}
	
	/**
	 * Gets the left child of this node
	 * @return the left child of this node
	 */
	public TrafficNode getLeft() {
		return leftChild;
	}
	
	/**
	 * Gets the right child of this node
	 * @return the right child of this node
	 */
	public TrafficNode getRight() {
		return rightChild;
	}
	
	/**
	 * Sets the left child of this node
	 * @param newNode this node's new left child
	 */
	public void setLeft(TrafficNode newNode) {
		leftChild = newNode;
	}
	
	/**
	 * Sets the right child of this node
	 * @param newNode this node's new right child
	 */
	public void setRight(TrafficNode newNode) {
		rightChild = newNode;
	}
	
	/**
	 * Adds a speed for cars heading North
	 * @param speed the speed to add
	 */
	public void setSpeed(double speed, int direction) {
		if (direction == 1) // NORTH
			northSpeeds.add(speed);
		if (direction == 3) // SOUTH
			southSpeeds.add(speed);
	}
	
	/**
	 * Gets the average of the speeds of cars heading North
	 * @return the average
	 */
	public double getAverageNorthSpeed() {
		int total = 0;
		
		for (int i = 0; i < northSpeeds.size(); i++)
			total += northSpeeds.get(i);		
		if (northSpeeds.size() == 0)
			return -2;
		else
			return (total / northSpeeds.size());
	}
	
	/**
	 * Gets the average of the speeds of cars heading South
	 * @return
	 */
	public double getAverageSouthSpeed() {
		int total = 0;
		
		for (int i = 0; i < southSpeeds.size(); i++)
			total += southSpeeds.get(i);		
		if (southSpeeds.size() == 0)
			return -2;
		else
			return (total / southSpeeds.size());
	}
	
	/** 
	 * Testing to make sure values are correct
	 */
	public String toString() {
		return roadName;
	}
}
