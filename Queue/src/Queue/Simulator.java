import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.MatteBorder;
import javax.swing.table.DefaultTableModel;

/*
 * 
 * Kevin Young
 * 109561787
 * Homework 4
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

/**************        How I check for vacation days in O(1) time         *********************************************
 * 																													  *		
 *     Because the vacationDays array is sorted in ascending order, the following process works. 		              *
 *     Keep a vacation counter (called vacCount) that starts at 0. For each day in the simulation,                    *
 *     check if today is equivalent to vacationDays[vacCount]. Once a vacation day is hit, vacCount                   *
 *     increments by one. So, if the vacationDays array contains {3, 5, 10, 15} then we start                         *
 *     looking at 3 when the simulation starts. Once the simulation clock is at this day, we mark today               *
 *     as a vacation day, and then look a 5 until the simulation clock is at this day, and so on.                     *
 *     This process is continued until vacCount is greater than or equal to the length of the vacationDays array      */	

/** 
 * A simulation of the staff of a large company as they complete several projects over 
 * the course of several days. Each day, there is a certain probability that a new project 
 * will come in. New projects are placed on a queue to be completed when employees become 
 * available. The company has vacation days which are specified prior to running the simulation.
 * The time unit of this simulation will be one day. The class will not use specific hard-coded numbers, 
 * so simulations with different parameters can be run easily
 * 
 * @author Kevin
 */
public class Simulator extends Project {

	private static boolean runAgain = true;
	private int numEmployees;  			// number of employees working for the company
    private int minProjectTime;		 	// minimum amount of time a project must require to complete
    private int maxProjectTime;	 		// maximum amount of time a project can require to complete
    private int maxProjectEmployees; 	// maximum number of employees a project can require to be worked on
    private double arrivalProb; 		// probability of a project arriving on each day
    private int[] vacationDays; 		// list of days which are vacations for the company	(no work is done on these days)  
	private int numFreeEmployees;       // number of employees who are currently not assigned to any project	
	private int projectsCompleted;      // number of projects completed in the simulation so far
	private int totalTimeWaited = 0;    // total time waited in the queue by all projects in the simulation so far
	private ArrayList<Project> activeProjects = new ArrayList<Project>(); // list of projects currently being worked on
	private ProjectQueue queue = new ProjectQueue();                      // list of projects waiting to be worked on
	private boolean vacationsDone = false;                                // whether or not all of the vacation days have passed
	
	/**
	 * Creates a new Simulator with the given parameters
	 * @param numEmployees  number of employees working for the company
	 * @param minProjectTime minimum amount of time a project must require to complete
	 * @param maxProjectTime maximum amount of time a project can require to complete
	 * @param maxProjectEmployees maximum number of employees a project can require to be worked on
	 * @param arrivalProb probability of a project arriving on each day 
	 * @param vacationDays list of days which are vacations for the company
	 * @throws IllegalArgumentException if the given values are incorrect
	 */
	public Simulator (int numEmployees, int minProjectTime, int maxProjectTime, 
			int maxProjectEmployees, double arrivalProb, int[] vacationDays) {
		if (numEmployees < 1)
			throw new IllegalArgumentException("There must be atleast one employee working for the company");
		else
			this.numEmployees = numEmployees;
		
		if (minProjectTime <= 0)
			throw new IllegalArgumentException("The minimum project time must be a positive value");
		else
			this.minProjectTime = minProjectTime;
		
		if ((maxProjectEmployees > numEmployees) || maxProjectEmployees < 1 )
			throw new IllegalArgumentException("The maximum amount of employees on a project must be less than the number of employees" +
					" and greater than one");
		else
			this.maxProjectEmployees = maxProjectEmployees;
		
		if (maxProjectTime < minProjectTime)
			throw new IllegalArgumentException("The maximum project time must be greater than or equal to the minimum project time");
		else
			this.maxProjectTime = maxProjectTime;
		
		if (arrivalProb < 0.0 || arrivalProb > 1.0)
			throw new IllegalArgumentException("The arrival probability must be greater than 0.0 and less than 1.0");
		else
			this.arrivalProb = arrivalProb;
		
		this.vacationDays = vacationDays;
	}
	
	/**
	 * Sorts the vacationDays array
	 * @param vacationDays list of days that no work will be done on
	 * @return the sorted vacationDays array
	 */
	public static int[] sortVacationDays(int[] vacationDays) {
		Arrays.sort(vacationDays);
		return vacationDays;
	}
	
	/**
	 * Randomly decides how long the project will take to complete
	 * @param minProjectTime the minimum amount of time a project must require
	 * @param maxProjectTime the maximum amount of time a project can require
	 * @return a random integer probability in the range [minProjectTime, maxProjectTime]
	 * @throws IllegalArgumentException if either given time is negative
	 */
	public int projectTime(int minProjectTime, int maxProjectTime) {
		if ((minProjectTime < 0) || (maxProjectTime < 0)) 
			throw new IllegalArgumentException("The times must be positive");
		return minProjectTime + (int)(Math.random() * ((maxProjectTime - minProjectTime) + 1));
	}
	
	/**
	 * Random decides how many employees will be needed 
	 * @param maxProjectEmployees the maximum amount of time a project can require
	 * @return a random integer in the range [1, maxProjectEmployees]
	 * @throws IllegalArgumentException if maxProjectEmployees is greater than numEmployees or maxProjectEmployees is less than one
	 */
	public int employeesNeeded(int maxProjectEmployees) {
		if ((maxProjectEmployees > numEmployees) || maxProjectEmployees < 1)
			throw new IllegalArgumentException("The maximum amount of employees on a project must be less than the number of employees" +
					"and greater than one");
		return (int) (Math.random() * maxProjectEmployees + 1);
	}
	
	/**
	 * Print projects currently being worked on in the format [(timeEntered, numberOfEmps, numberOfDays)]
	 * @param projects the arrayList to print 
	 */
	public void printActiveProjs(ArrayList<Project> projects) {
		System.out.print("Active projects: [");
		if (projects.size() != 0) {
			for (int i = 0; i < projects.size(); i++) {
				System.out.print("(" + projects.get(i).getTimeEntered() + ", " + projects.get(i).getNumberOfEmps()
									+ ", " + projects.get(i).getNumberOfDays() + ")");
				if (i != projects.size()-1)
					System.out.print(", ");
			}
			System.out.print("]");
		}
		else
			System.out.print("]");
		System.out.println();
	}
	
	/**
	 * Print projects waiting to be worked on in the format [(timeEntered, numberOfEmps, numberOfDays)]
	 * @param projects the arrayList to print 
	 * @throws EmptyQueueException 
	 */
	public void printQueue(ProjectQueue projects) {
		System.out.print("Project queue: [");
		if (projects.size() != 0) {
			for (int i = 0; i < projects.size(); i++) {
				System.out.print("(" + projects.getProjQueue().get(i).getTimeEntered() + ", " + projects.getProjQueue().get(i).getNumberOfEmps()
									+ ", " + projects.getProjQueue().get(i).getNumberOfDays() + ")");
				if (i != projects.size()-1)
					System.out.print(", ");
			}
			System.out.print("]");
		}
		else
			System.out.print("]");
		System.out.println();
	}	
	
	
	/**
	 * Runs a simulation with this Simulator's parameters for the given duration of time
	 * @param duration how long this simulation will last
	 * @return the average time each project spent in the queue
	 * @throws EmptyQueueException if an attempt to dequeue an empty queue is made
	 */
	public double simulate(int duration) throws EmptyQueueException {
	
		System.out.println();
		Scanner input = new Scanner(System.in);
		boolean pauseSimulation = false; // if this is true, then the user is given the option to be shown the GUI
		System.out.println("Please select one of the following (1/2): \n1. Run simulation normally \n2. Run and incorporate GUI");
		int response = input.nextInt();
		if (response == 1)
			pauseSimulation = false;
		if (response == 2)
			pauseSimulation = true;
		System.out.println();
		int day = 1; 	  			     // start the simulation at day 1
		int vacCount = 0; 				 // when we hit a vacation day, the counter goes up. We look at the sorted
					      				 // vacationArray at index vacationCounter to check if we're at a vacation day
		numFreeEmployees = numEmployees; // at the beginning of the simulation, the number of free employees equals the amount of employees\
		
		while (day <= duration) {			
			System.out.println("Day " + day);
			
			/* Check if the current day is a vacation day */
			boolean isVacation = false;
			if (vacCount >= vacationDays.length) {
				vacationsDone = true;    // once vacCount is at least the size of vacationDays, then we know every vacation day has been passed
				isVacation = false;
			}
                        
			if (!vacationsDone)
				if (day == vacationDays[vacCount]) {
					System.out.println("Vacation day\n");				
					vacCount++;
					day++;
					isVacation = true;
					continue; 			// do nothing and go to next day if current day is a vacation day
				}
			
			/* If today is not a vacation day, perform all operations normally */
			if (!isVacation) {
				BooleanSource arrival = new BooleanSource(arrivalProb); 			// decide whether or not a project will arrive in this time unit using arrivalProb
				if (arrival.occurs()) { 							    		    // if a project occurs based on arrivalProb
					int timeToComplete; 											// how long the project will take to complete
					timeToComplete = projectTime(minProjectTime, maxProjectTime);
					int employeesNeeded;											// how many employees will be needed for the project
					employeesNeeded = employeesNeeded(maxProjectEmployees); 		
					Project project = new Project(day, employeesNeeded, timeToComplete); 
					queue.enqueue(project);											// add this project to the list of projects waiting to be worked on
					if (employeesNeeded == 1)									    // this is merely to determine whether to print "employee" or "employees"
						System.out.print("A project arrives requiring " + employeesNeeded + " employee for ");
					else
						System.out.print("A project arrives requiring " + employeesNeeded + " employees for ");
					if (timeToComplete == 1)										// this is merely to determine whether to print "day" or "days"
						System.out.println(timeToComplete + " day");
					else
						System.out.println(timeToComplete + " days");
				}
				else 																
					System.out.println("No projects occur");	
				
				/* Check if there are enough employees available to start working on the project at the head of the queue */
				if (!queue.isEmpty()) {
					if (numFreeEmployees >= queue.peek().getNumberOfEmps()) { 
						numFreeEmployees -= queue.peek().getNumberOfEmps();   
						activeProjects.add(queue.dequeue());                  
					}			
				}
				/* Go through the list of active projects and decrement the time remaining on each */
				boolean projectCompleted = false; // whether or not a project has been completed
				int projectsCompCounter = 0; 				  // counts how many completed projects at this instance
				int[] timeWaitedArr = new int[activeProjects.size()]; // this array will hold how much time was waited by each completed project
				for (int i = 0; i < activeProjects.size(); i++) {
					activeProjects.get(i).setNumberOfDays(activeProjects.get(i).getNumberOfDays()-1); // decrement the time remaining by one
					if (activeProjects.get(i).getNumberOfDays() == 0) { 	         // handling a completed project
						projectCompleted = true;
						int timeEntered = activeProjects.get(i).getTimeEntered();    // the day this completed project was entered into the simulation
						totalTimeWaited += day - timeEntered;   			         
						projectsCompleted++;
						numFreeEmployees += activeProjects.get(i).getNumberOfEmps(); // return the employees to the pool of free employees
						activeProjects.remove(i); 				    			     
						i--; 	                                    			     // the size of activeProjects has gone down one, so must adjust i such that the loop doesn't exit early
						timeWaitedArr[projectsCompCounter] = day - timeEntered; 	
						projectsCompCounter++;							
					}
				}
				
				/* Print all the active projects, and print how much time each completed project waited. This is done this way for printing formatting */
				if (projectCompleted) {
					printActiveProjs(activeProjects);
					for (int i = 0; i < projectsCompCounter; i++) {
						if (timeWaitedArr[i] == 1)
							System.out.println("A project was completed. It arrived in the queue " + timeWaitedArr[i] + " day ago.");
						else
							System.out.println("A project was completed. It arrived in the queue " + timeWaitedArr[i] + " days ago.");
					}
				}
				else
					printActiveProjs(activeProjects);
				
				/* Display the number of projects completed and the total time waited if a project was completed */
				if (projectsCompleted != 0 && projectCompleted)
					System.out.println("Projects completed: " + projectsCompleted);
				if (totalTimeWaited != 0 && projectCompleted)
					System.out.println("Total time waited: " + totalTimeWaited);
				
				System.out.println("Free Employees: " + numFreeEmployees);
				printQueue(queue);
			}
			
			/* If the user prompted to run the GUI, then present them with a menu */
			if (pauseSimulation) {
				System.out.println("\nPlease select one of the following (1/2/3/4): \n1. Continue to next day \n2. Graphically show status of active projects \n" +
						"3. Graphically show status of projects in queue \n4. Run rest of simulation as normal");
				int intResponse = input.nextInt();
				boolean isDone = false;
				switch (intResponse) {
					case 1:  break;					
					case 2:  displayQueueStatus(activeProjects);							 
							 while (!isDone) {
								 System.out.println("\nEnter \"done\" to continue, or enter \"3\" to show the queue: ");
								 input.nextLine();
								 String responseStr = input.next();
								 if (responseStr.equalsIgnoreCase("done")) {
									 isDone = true;
								 }
								 else if (responseStr.equals("3"))
									 displayQueueStatus(queue.getProjQueue());
								 else 
									 System.out.println("I didn't quite catch that.");
							 }							 
							 break;
					case 3:  displayQueueStatus(queue.getProjQueue());	
						     while (!isDone) {
								 System.out.println("\nEnter \"done\" to continue, or enter \"2\" to show the active projects: ");
								 input.nextLine();
								 String responseStr = input.next();
								 if (responseStr.equalsIgnoreCase("done")) {
								 	isDone = true;
								 }
								 else if (responseStr.equals("2"))
									 displayQueueStatus(activeProjects);
								 else 
								 	System.out.println("I didn't quite catch that.");
					 		  }							 
					 		 break;
					case 4:  pauseSimulation = false;
							 break;
					default: pauseSimulation = false;
						     break;
				}				
			}
			System.out.println();
			day++;
		}
		if (projectsCompleted == 0 || projectsCompleted > 1)						// this is merely to determine whether to print "project" or "projects"
			System.out.print(projectsCompleted + " projects completed. The average waiting time was ");
		if (projectsCompleted == 1)
			System.out.print(projectsCompleted + " project completed. The average waiting time was ");
		if (projectsCompleted == 0)
			return 0;
		else
			return (double)totalTimeWaited / projectsCompleted; 					// return the average time each project spent in the queue
	 }
	
	/* As long as the user doesn't select to terminate the program, keep displaying a menu to the
	 *  user which will give options to create a simulation
	 */
	public static void main(String[] args) throws EmptyQueueException {
		while (runAgain)
			printMenu();		
	}
	
	/**
	 *  Prompts the user for the simulation parameters and duration of simulation, instantiates a Simulator,
	 *  runs the simulation, displays the results, then gives the user the option of either running another 
	 *  simulation, or quitting.
	 */
	public static void printMenu() throws EmptyQueueException {		
			Scanner input = new Scanner(System.in);
			
			System.out.print("Enter the number of employees: ");
			int numEmployees = input.nextInt();
			
			System.out.print("Enter the minimum project time: ");
			int minProjectTime = input.nextInt();
			
			System.out.print("Enter the maximum project time: ");
			int maxProjectTime = input.nextInt();
			
			System.out.print("Enter the maximum number of employees for a project: ");
			int maxNumEmployees = input.nextInt();
			
			System.out.print("Enter the arrival probability: ");
			double arrivalProb = input.nextDouble();
			
			System.out.print("Enter the list of vacation days, separated by space. Enter 0 for no vacation days: ");
			input.nextLine(); // to appease Scanner, the input is not taken from the user until the next line
			String vacationResponse = input.nextLine();
			boolean noVacationDays = false; 
			int[] vacationDays;
			
			if (!vacationResponse.equals("0")) {
				String[] vacationDaysStr = vacationResponse.split(" ");
				vacationDays = new int[vacationDaysStr.length];
				for (int i = 0; i < vacationDaysStr.length; i++) 
					vacationDays[i] = Integer.parseInt(vacationDaysStr[i]);
				vacationDays = sortVacationDays(vacationDays); // sorted vacationDays array
			}
			else {
				noVacationDays = true;
				vacationDays = new int[1];
				vacationDays[0] = 0;
			}
			
			try {
				Simulator simulate = new Simulator(numEmployees, minProjectTime, maxProjectTime, maxNumEmployees, 
					arrivalProb, vacationDays);
				System.out.print("Enter the duration of the simulation: ");
				int duration = input.nextInt();
				if (duration < 1)
					throw new IllegalArgumentException("The duration must be atleast one");
				
				if ((vacationDays[0] <= 0 || (vacationDays[vacationDays.length-1] > duration)) && !(noVacationDays))  // throw an exception if there is a vacation day that doesn't even get passed in the duration
					throw new IllegalArgumentException("All of the vacation days must occur within the duration of the simulation");
				
				System.out.printf("%.2f", simulate.simulate(duration));
				System.out.println(" days per project");
				boolean askAgain = true; 							   // whether or not to re-prompt the user
				while (askAgain) {
					System.out.print("\nRun another simulation? (Y/N): ");
					switch (input.next().toUpperCase()) {
						case "Y": runAgain = true;
								  askAgain = false;
								  System.out.println();
								  break;
						case "N": System.out.println("Terminating..");	
							      System.exit(0);
							      break;
						default:  System.out.println("Invalid input");
					}	
				}
			} catch (IllegalArgumentException e) {					    // if an exception is caught, then re-prompt the user
				System.out.println("\n" + e.getMessage() + "\n");
			}
		}
	
	/**
	 * Creates a GUI which allows the user to select the option of creating a JTable to display the contents of queue
	 * @param queue the queue where the displayed contents will come from
	 */
	public static void displayQueueStatus(ArrayList<Project> queue) {
		SimulatorGUI simulatorGUI = new SimulatorGUI();
		simulatorGUI.setQueue(queue);
	}

/**
 * This class will create a GUI which allows the user to see the contents of a queue of Project objects through a JTable	
 * @author Kevin 
 */
static class SimulatorGUI extends JFrame {		
		
		private String[] columnNames = new String[] {"Position", "Day Entered", "Number of Employees", "Days Remaining"};
		private ArrayList<Project> queue; 						// the queue that will be presented through the JTable
	
		DefaultTableModel model;
		JTable table;
		JPanel panel1 = new JPanel();
		JPanel panel2 = new JPanel();
		JButton a = new JButton("Show the queue");
		JButton x = new JButton("Close GUI");

		/**
		 * Sets the queue that will be presented in the JTable
		 * @param queue the queue to be presented in the JTable
		 */
		public void setQueue(ArrayList<Project> queue) {
			this.queue = queue;
		}

		/**
		 * Returns the queue that will be presented in the JTable
		 * @return queue the queue to be presented in the JTable
		 */
		public ArrayList<Project> getQueue() {
			return queue;
		}
		
		/**
		 * Creates a JFrame with two buttons, one to display to queue, and the other to close the GUI
		 */
		public SimulatorGUI() {

			getContentPane().setBackground(Color.DARK_GRAY);
			setTitle("Simulator");
			getContentPane().setLayout(new GridBagLayout());
			getContentPane().add(panel1);

			/* Adding buttons and editing of the first JPanel */
			panel1.setBackground(Color.DARK_GRAY);
			panel1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));	

			panel1.setBackground(Color.DARK_GRAY);
			panel1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			a.setBounds(12, 69, 288, 29);
			getContentPane().add(a);

			/* Formatting JButtons */
			a.setFont(new Font("Narkisim", Font.PLAIN, 20)); // formatting JButton a
			a.setBackground(Color.CYAN);
			a.setVerticalAlignment(SwingConstants.TOP);

			panel2.setBounds(0, 124, 340, 10);
			panel2.setBackground(Color.DARK_GRAY);
			getContentPane().add(panel2);
			x.setBounds(294, 13, 122, 29);
			getContentPane().add(x);		

			x.setFont(new Font("Narkisim", Font.PLAIN, 20)); // formatting JButton x
			x.setBackground(Color.CYAN);
			x.setVerticalAlignment(SwingConstants.TOP);	

			/* Adding ActionListeners */
			a.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e)  {
					if (getQueue().size() == 0) {
						UIManager UIM = new UIManager();
						UIM.put("OptionPane.background", Color.DARK_GRAY);
						UIM.put("Panel.background", Color.DARK_GRAY);
						String text = "<html> <span style='color:black'>This queue is empty</span></html>";
						JOptionPane.showMessageDialog(null, text, "Empty Queue Warning", JOptionPane.WARNING_MESSAGE);
					}
					else
						createTable(getQueue());
				}
			});
			x.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e)  {
					dispose();							     // close this GUI without exiting the program
				}
			});

			this.setSize(473,100);
			setVisible(true);
		}		
		
		/**
		 * Creates a JTable that will display the contents of the given queue
		 * @param queue the queue to display the contents of
		 */
		public void createTable(ArrayList<Project> queue) {
			
			model = new DefaultTableModel(columnNames, 100);	
			table = new JTable(model) {
				public boolean isCellEditable(int rowIndex, int mColIndex) { // makes the rows and columns uneditable
					return false;
				}
			};			
			
			table.getColumnModel().getColumn(2).setPreferredWidth(100);
			table.setRowSelectionAllowed(false);
			table.setBackground(Color.GRAY);
			table.setBorder(new MatteBorder(1, 1, 1, 1, (Color) Color.BLUE));
			table.setFont(new Font("Narkisim", Font.PLAIN, 15));
			JScrollPane	pane = new JScrollPane(table);
			
			/* Sets the proper row and column to display the contents of the respective Project inside the queue */
			for (int i = 0; i < queue.size(); i++) {
				if (i == 0) {									     // if the current project in the queue is the first project, then it is the front
					if (queue.size() == 1)							 
						table.setValueAt("Front/Rear",0,0);
					else
						table.setValueAt("Front",0,0);
					setTable(queue, 0);
				}
				else if (i == queue.size()-1 && queue.size() != 1) { // if the current project in the queue is the last project, then it is the rear
					table.setValueAt("Rear",queue.size()-1,0);
					setTable(queue, queue.size()-1);
				}
				else {
					table.setValueAt(i+1, i, 0);
					setTable(queue, i);
				}
			}
			add(pane);
			setVisible(true);
			setSize(580,420);
			setLayout(new FlowLayout());
			setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		}
		
		/**
		 * Sets the row and column ti display the contents of the respective Project
		 * @param queue the queue to be displayed
		 * @param i the row where this project will be displayed
		 */
		public void setTable(ArrayList<Project> queue, int i) {
			table.setValueAt(queue.get(i).getTimeEntered(), i, 1);
			table.setValueAt(queue.get(i).getNumberOfEmps(), i, 2);
			table.setValueAt(queue.get(i).getNumberOfDays(), i, 3);
		}
	}
}