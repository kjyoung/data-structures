import java.util.ArrayList;

/*
 * 
 * Kevin Young
 * 109561787
 * Homework 4
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

/** 
 * This class, which is implementing the Queue interface, will represent the list of pending
 * projects (projects waiting to be completed).
 * The "head" of the queue (or the front) will refer to index 0 of the queue.
 * The "tail" of the queue (or the rear) will refer to index (size - 1) of the queue.
 * Following queue standards, this class follows a FIFO algorithm.
 * 
 * @author Kevin
 */

public class ProjectQueue extends ArrayList {
	
	private ArrayList<Project> projQueue = new ArrayList<Project>(); // will hold all the projects in the queue
	private int front; // the "head" of the queue
	private int rear;  // the "tail" of the queue
	
	/**
	 * Creates an empty ProjectQueue
	 */
	public ProjectQueue() {
		front = -1;
		rear = -1;
	}
	
	/** 
	 * Places a project at the end of the queue
	 * @param c the Project to enter into the queue
	 */
	public void enqueue(Project c) {
		rear++;
		front = 0;
		projQueue.add(rear, c); // add the project to the tail of projQueue		
	}
	
	/**
	 * Removes and returns the project at the front of the queue
	 * @return the project at the front of the queue
	 */
	 public Project dequeue() throws EmptyQueueException {
		 if (rear == -1) 
			 throw new EmptyQueueException("This project queue is empty. Can not remove any project");
		 else {
			 rear--;
			 Project data = projQueue.get(front);
			 projQueue.remove(front); // remove the front of the queue
			 return data; // return the front of the queue
		 }		 
	 }
	 
	 /**
	  * Returns the project at the head of the queue without removing it
	  * @return the project at the head of the queue
	  */
	 public Project peek() {
		 return projQueue.get(front);
	 }
	 
	 /** 
	  * Returns the number of projects in the queue
	  * @return the number of projects in the queue
	  */
	 public int size() {
		 return rear+1;
	 }
	 
	 /** 
	  * Determines if the queue is empty
	  * @return true if the list is empty. Otherwise return false
	  */
	 public boolean isEmpty() {
		 return rear == -1;
	 }
	 
	 /**
	  * Determines if two ProjectQueue objects are equal to each other. They're equal if the
	  * time entered, the number of employees needed, and the number of days required of each
	  * Project inside the two queue's are equivalent.
	  * @param obj the object to compare the current ProjectQueue object's contents with
	  * @return true if the two ProjectQueue objects are equal. Otherwise return false
	  */
	 public boolean equals(Object obj) {
		 ProjectQueue pqObj = (ProjectQueue)obj;
		 if (pqObj.size() != this.size()) {
			 return false; // return false if the sizes are different
		 }
		 for (int i = 0; i < this.size(); i++) {
			if (pqObj.projQueue.get(i).getTimeEntered() == this.projQueue.get(i).getTimeEntered()
				  && pqObj.projQueue.get(i).getNumberOfEmps() == this.projQueue.get(i).getNumberOfEmps()
				  && pqObj.projQueue.get(i).getNumberOfDays() == this.projQueue.get(i).getNumberOfDays()) {
				if (i == this.size()-1) 
					return true;  // return true if every element of projQueue has been compared, and each have been equal
				continue;
		 	}
			else 
				return false;					 
		 }
		 return false; // to appease the compiler
	 }
	 
	 /**
	  * Return the array list of Project objects
	  * @return the array list of Project objects
	  */
	 public ArrayList<Project> getProjQueue() {
		 return projQueue;
	 }	 
}