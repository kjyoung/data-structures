/*
 * 
 * Kevin Young
 * 109561787
 * Homework 4
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

/**
 * This class will use a given probability to determine if something will occur
 * 
 * @author Kevin
 */
public class BooleanSource
{
	private double probability;
	
	public BooleanSource(double p) throws IllegalArgumentException {
		if (p < 0.0 || p > 1.0)
			throw new IllegalArgumentException("The probability must be greater than 0.0 and less than 1.0");
		probability = p;
	}
	
	public boolean occurs() {
		return (Math.random() < probability);
	}	
}