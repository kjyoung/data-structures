/*
 * 
 * Kevin Young
 * 109561787
 * Homework 4
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

public class EmptyQueueException extends Exception {
	
	public EmptyQueueException(String message) {
		super(message);
	}
}
