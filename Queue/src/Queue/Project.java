/*
 * 
 * Kevin Young
 * 109561787
 * Homework 4
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

/**
 * This class represents one project. Each project keeps track of the time on the simulation
 * clock when it entered the queue of pending projects, the number of employees required to 
 * work on the project, and the number of days it will take to complete the project (both random
 * numbers to be generated when the project is created)
 * 
 * @author Kevin
 */
public class Project {
	
	private int timeEntered;  // time on the simulation clock the project entered the queue of pending projects
	private int numberOfEmps; // number of employees required to work on the project
	private int numberOfDays; // number of days it will take to complete the project
	
	/**
	 * Creates a new Project
	 */
	public Project() {}
	
	/**
	 * Creates a new Project with the given parameters
	 * @param timeEntered time on the simulation clock the project entered the queue of pending projects
	 * @param numberOfEmps number of employees required to work on the project
	 * @param numberOfDays number of days it will take to complete the project
	 */
	public Project(int timeEntered, int numberOfEmps, int numberOfDays) {
		this.timeEntered = timeEntered;
		this.numberOfEmps = numberOfEmps;
		this.numberOfDays = numberOfDays;
	}
	
	/**
	 * Sets the time this project was entered into the simulation clock 
	 * @param timeEntered time on the simulation clock the project entered the queue of pending projects
	 */
	public void setTimeEntered(int timeEntered) {
		this.timeEntered = timeEntered;
	}
	
	/**
	 * Sets the number of employees needed to work on this project
	 * @param numberOfEmps number of employees required to work on the project
	 */
	public void setNumberOfEmps(int numberOfEmps) {
		this.numberOfEmps = numberOfEmps;
	}
	
	/**
	 * Sets the number of days it will take to complete the project
	 * @param numberOfDays number of days it will take to complete the project
	 */
	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}
	
	/**
	 * Returns the time this project was entered into the simulation clock 
	 * @return time on the simulation clock the project entered the queue of pending projects
	 */
	public int getTimeEntered() {
		return timeEntered;
	}
	
	/**
	 * Returns the number of employees needed to work on this project
	 * @return number of employees required to work on the project
	 */
	public int getNumberOfEmps() {
		return numberOfEmps;
	}
	
	/**
	 * Returns the number of days it will take to complete the project
	 * @return number of days it will take to complete the project
	 */
	public int getNumberOfDays() {
		return numberOfDays;
	}
}