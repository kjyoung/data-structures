# README #

### Description ###

This repo contains assignments from CSE 214 - a course at Stony Brook University that focuses on data structures using Java. These assignments are small, simple, and are meant to show an understanding of several data structures.

These assignments are from my Fall 2014 semester.