/*
 * 
 * Kevin Young
 * 109561787
 * Homework 6
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.io.Serializable;

/**
 * This class holds data for a particular car
 * @author Kevin
 */
public class CarStatus implements Serializable {
	
	private int id; // a unique identifier for this car
	private String road; // the name of the road the car is currently on
	private double position; // the position of the car along the road 
	private double time; //  the time that this data was captured
	private double speed; //  the speed of the car, if known (to calculate the speed of a car requires its position at two different times on the same road). Otherwise, -1.
	
	public CarStatus(int id, String road, double position, double time) {
		this.id = id;
		this.road = road;
		this.position = position;
		this.time = time;
	}
	
	/**
	 * Returns This car's ID, which distinguises itself from other cars
	 * @return The id number
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Set this car's ID, which distinguises itself from other cars
	 * @param id The id number
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Return the road this car is on
	 * @return road The road this car is on
	 */
	public String getRoad() {
		return road;
	}
	
	/**
	 * Set the road this car is on
	 * @param road The road this car is on
	 */
	public void setRoad(String road) {
		this.road = road;
	}
	
	/**
	 * Returns the car's distance from some predefined point on the road
	 * @return The position of the car
	 */
	public double getPosition() {
		return position;
	}
	
	/**
	 * Set the car's distance from some predefined point on the road
	 * <dt><b>Precondition:</b><dd>
	 * Position must be greater than 0
	 * @param position The position of the car
	 */
	public void setPosition(double position) {
		this.position = position;
	}
	
	/**
	 * Gets the time this car was added to a road
	 * @return The time this car was added
	 */
	public double getTime() {
		return time;
	}
	
	/**
	 * Sets the time this car was added to a road
	 * @param time The time this car was added
	 */
	public void setTime(double time) {
		this.time = time;
	}
	
	/**  
	 * Returns the current speed of the road
	 * Speed is unknown until it has two positions at two different times on the same road
	 * @return The speed
	 */
	public double getSpeed() {
		return speed;
	}
	
	/**
	 * To calculate the speed of car requires its position at two different times on the same road.
	 * <dt><b>Precondition:</b><dd>
	 * The position at two different times on the same road must be known
	 * @param speed The speed of the car. 
	 *        		If this is unknown, then speed is set to -1
	 */
	public void setSpeed(double speed) {
		this.speed = speed;
	}
}
