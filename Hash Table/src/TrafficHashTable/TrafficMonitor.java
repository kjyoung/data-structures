/*
 * 
 * Kevin Young
 * 109561787
 * Homework 6
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.util.List;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Hashtable;

@SuppressWarnings("serial")
public class TrafficMonitor implements Serializable {
	
	private Hashtable<Integer, CarStatus> cars; //  a Hashtable mapping car ID numbers to CarStatus objects representing the last known status of that car.
	private Hashtable<String, List<Integer>> roads; //  a Hashtable mapping road names to a list of the ID numbers of all cars on that road.
	public static ArrayList<Integer> knownId = new ArrayList<Integer>(); // holds the index of car ID's
	
	/**
	 * Create a TrafficMonitor with an empty cars and roads Hashtable
	 */
	public TrafficMonitor() {
		cars  = new Hashtable<Integer, CarStatus>();
		roads = new Hashtable<String, List<Integer>>();
	}
	
	/**
	 * Add data for one car.
	 * @param id The identification number of this car
	 * @param road The road this car will be added to
	 * @param position The position this car will be added at
	 * @param time The time this car was added
	 */
	public void addData(int id, String road, double position, double time) {
		
		if (position < 0 || time < 0)
			throw new IllegalArgumentException("Cannot have negative values for position or time.");
		
		// Handle a car that does not yet exist
		if (!cars.containsKey(id)) { // if this car does not exist yet
			insertNewCar(id, road, position, time);
			return;
		}
		
		/* Handle a car that currently exists (update its data in the cars hashtable) */
		double oldTime = cars.get(id).getTime();
		if (time < oldTime) // data should arrive in temporal order for each car) 
			throw new IllegalArgumentException("The new time must be greater or equal to the old time of this car.");
		
		double oldPosition = cars.get(id).getPosition();
		if (position < oldPosition) // all cars will be traveling in the same direction
			throw new IllegalArgumentException("The new position must be greater than or equal to the old position of this car");
		
		// Handle if the car HAS changed roads
		String oldRoad = cars.get(id).getRoad(); 
		boolean changedRoad = false;
		if (!oldRoad.equals(road)) {
			carChangedRoads(oldRoad, road, id);	
			changedRoad = true;
		}
		
		// Update the data of this car
		updataCarData(id, position, oldPosition, time, oldTime, road, changedRoad);			
	}
	
	/** 
	 * Updates the data of a car in the cars hashtable
	 * @param id The identification for this car
	 * @param position The position this car will be added at
	 * @param oldPosition The position this car was at
	 * @param time The new time this car was added
	 * @param oldTime The previous time that this car was added
	 * @param road The road to add this car to
	 */
	private void updataCarData(int id, double position, double oldPosition, double time, double oldTime, String road, boolean changedRoad) {
            if (!changedRoad) { // only set the speed based on position if the car has not changed road
		cars.get(id).setSpeed((position - oldPosition) / (time - oldTime));
            }
            cars.get(id).setRoad(road);		
            cars.get(id).setPosition(position);	
            cars.get(id).setTime(time);
	}
	
	/**
	 * If a car has changed roads, remove it from the old road, reset the speed,
	 * and add it to the new road
	 * @param oldRoad The road this car was on
	 * @param newRoad The road this car will be added to
	 * @param id The car to add
	 */
	private void carChangedRoads(String oldRoad, String newRoad, int id) {
		Integer idObj = id;
		roads.get(oldRoad).remove(idObj); // remove this car from the road it just changed from
		cars.get(id).setSpeed(-1); // a car that just switched roads has no speed
		addToRoads(newRoad, id);
	}
	
	/**
	 * Adds a new car to the hashtables if the car does not already exist
	 * @param id The car to add
	 * @param road The road to be added to
	 * @param position The position that this car will be added at
	 * @param time The time this car was added
	 */
	private void insertNewCar(int id, String road, double position, double time) {
		CarStatus newCar = new CarStatus(id, road, position, time);
		newCar.setSpeed(-1);
		cars.put(id, newCar);
		addToRoads(road, id);
	}
	
	/**
	 * Adds the car with the given id to the given road
	 * @param road The road to be added to
	 * @param id The car to be added
	 */
	private void addToRoads(String road, int id) {
		try {
			roads.get(road).isEmpty(); 
			roads.get(road).add(id);
		} catch (NullPointerException e) { // means this list does not exist..yet..
			ArrayList<Integer> roadList = new ArrayList<Integer>();
			roadList.add(id);
			roads.put(road, roadList);
		}
	}
	/**
	 * Removes all data for the car with the ID number id from both hash tables.
	 * @param id The car to remove
	 */
	public void remove(int id) {
		if (carExists(id)) {
			Integer idObj = id;
			roads.get(cars.get(id).getRoad()).remove(idObj);
			cars.remove(id);
			System.out.println("Removed car #" + id);
		}
		else
			throw new IllegalArgumentException("Car #" + id + " is unknown.");
	}
	
	/**
	 * Checks to see this car exists in the cars Hashtable
	 * @param id The car to see if it exists
	 * @return true If the car exists, else false
	 */
	private boolean carExists(int id) {
		try {
			cars.get(id).getRoad();
			return true;
		} catch (NullPointerException e) {
			return false;
		}
	}
	/**
	 * Returns the name of the road that the car with ID number id is on, else return null if there is no car with that ID number.
	 * @param id The car ID
	 * @return roadName The name of the road with the ID number, else return null
	 *         
	 */
	public String getRoad(int id) {		
		if (!carExists(id))
			return null;
		
		return cars.get(id).getRoad();
	}
	
	/**
	 * Returns the speed of the car with ID number id
	 * @param id
	 * @return -1 if there is no car with this ID number
	 *         -2 if there is a car with this ID number, but its speed is unknown (because we have only one record of the car's location on its current road)
	 */
	public double getSpeed(int id) {
		if (!carExists(id))
			return -1;
		
		if (cars.get(id).getSpeed() == -1)
			return -2;
		
		return cars.get(id).getSpeed();		
	}

	/**
	 * Returns a List of the speed of each car currently on the indicated road, or null if the road doesn't exist. 
	 * If we don't know one of the cars's speeds, simply omit it from the returned list.
	 * @param road The road to look at
	 * @return An ArrayList holding all the speed values
	 */
	public List<Double> getKnownSpeeds(String road) {
		knownId.clear();
		ArrayList<Double> knownSpeeds = new ArrayList<Double>();
		
		if (roads.get(road) == null)
			throw new IllegalArgumentException("There are no cars on " + road);
		
		for (int u: roads.get(road)) {
			if (getSpeed(u) > -1) {
				knownSpeeds.add(getSpeed(u));
				knownId.add(u); // puts the ID of the car into knownID
			}
		}
		
		if (knownSpeeds.isEmpty()) 
			throw new IllegalArgumentException("There are no known speeds on " + road);
		
		return knownSpeeds;
	}

	/**
	 * Returns the average speed of cars on the indicated road. This method should not access the Hashtables directly; 
	 * rather it should call getKnownSpeeds and compute the average of that list.
	 * @param road The road to access
	 * @return A double giving the average speed
	 */
	public double getAverageSpeed(String road) {
		List<Double> knownSpeeds = getKnownSpeeds(road);
		
		if (knownSpeeds == null)
			return -1;
		
		double avgSpeed = 0;
		for (double d: knownSpeeds)
			avgSpeed += d;
		
		return (avgSpeed / knownSpeeds.size());
	}
	
	/**
	  * Opens a FileOutputStream and an ObjectOutputStream, and uses the ObjectOutputStream to write the TrafficMonitor to the 
	  * specified binary file, then the method closes the two output streams.
	  * @param filename the binary file to write the tree's content to
	  * @throws IOException 
	  */
	 public void serialize(String filename) {
		 ObjectOutputStream outStream = null;
		 try {
			 FileOutputStream file = new FileOutputStream(filename);
			 outStream = new ObjectOutputStream(file);
			 outStream.writeObject(this); // Writes this object to "filename".obj
			 outStream.close();
			 file.close();
		 }
		 catch (IOException e) {
			 System.out.println("Unable to write this object to the file \"" + filename + "\"");
		 }
	 }
	 
	 /**
	  * Opens a FileInputStream and an ObjectInputStream, and uses the ObjectInputStream to read a tree from the
	  * specified binary file, then closes the two output streams and returns the tree. Returns null 
	  * if the file was not found, or did not contain a valid tree object.
	  * @param filename the binary file where the tree's data will be read from
	  * <dt><b>Precondition:</b><dd>
	  *  The file was found, and if it does, the file contains a valid tree object
	  * @return the tree in the ObjectInputStream
	  * @throws IOException 
	  * @throws ClassNotFoundException 
	  */
	 public static TrafficMonitor deserialize(String filename) throws ClassNotFoundException, IOException {
		 TrafficMonitor monitor;
		 try {
		      FileInputStream file = new FileInputStream(filename);
		      ObjectInputStream inStream  = new ObjectInputStream(file);
		      monitor = (TrafficMonitor) inStream.readObject(); 
		      inStream.close();
		      file.close();
		 } catch (FileNotFoundException e) {
			 return null;
		 } catch (ClassNotFoundException e) {
			 return null;
		 } catch (ClassCastException e) { 
			 return null;
		 } catch (IOException e) {
			 return null;
		 }
		 return monitor;
	 }
	
	/**
	 * Gives access to the cars Hashtable
	 * @param id The car to retrieve
	 * @return A carStatus type
	 */
	public CarStatus getCars(int id) {
		return cars.get(id);
	}
	
}
