import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import javax.swing.JOptionPane;

/*
 * 
 * Kevin Young
 * 109561787
 * Homework 6
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

public class TrafficDriver {
	
	private static Scanner input = new Scanner(System.in);
	private static boolean runAgain = true;
	private static TrafficMonitor monitor = new TrafficMonitor();
	private static String filename = "traffic.obj";
	
	public static void main(String[] args) {
		loadMonitorFromFile();
		while (runAgain)
			printMenu();
	}
	
	/** Gets a tree object file and sets it as the tree, if the tree exists. */
	public static void loadMonitorFromFile() {
		TrafficMonitor newMonitor;
		try {
			newMonitor = TrafficMonitor.deserialize(filename);
			if (newMonitor != null) {
				System.out.println("Loaded tree from \"" + filename + "\"\n");
				monitor = newMonitor;
			} else
				System.out.println("\"" + filename + "\" not found or not a TrafficMonitor type. Using new monitor.\n");		
		} catch (ClassNotFoundException e) {
			System.out.println("\"" + filename + "\" not a TrafficMonitor type. Using a new monitor\n");
		} catch (IOException e) {
			System.out.println("\"" + filename + "\" not found\n");
		}		
	}
	
	/** Prints a menu which the user will choose from to perform an operation */
	public static void printMenu() {		
		
		System.out.println("A) Add data\n" +
				"R) Remove car\n" +
				"D) Get road\n" +
				"S) Get speed\n" +
				"T) Get all speeds on a road\n" +
				"U) Get average speed\n" +
				"Q) Quit and save to \"traffic.obj\"\n");
		
		System.out.print("Enter a selection: ");
		String selection = input.next().toUpperCase();
		
		switch (selection) {
			case "A": System.out.println();
					  addData();
					  System.out.println();
					  break;
			case "R": System.out.println();
					  removeCar();
					  System.out.println();
					  break;
			case "D": System.out.println();
					  getRoad();
					  System.out.println();
					  break;
			case "S": System.out.println();
					  getASpeed();
					  System.out.println();
					  break;
			case "T": System.out.println();
					  getAllSpeeds();
					  System.out.println();
					  break;
			case "U": System.out.println();
					  getAvgSpeed();
					  System.out.println();
					  break;
			case "Q": System.out.println();
					  quitAndSave();
					  System.out.println();
					  break;
		}		
	}
	
	/**
	 * Add a car and its data to a road
	 */
	public static void addData() {
		System.out.print("Enter the car ID number: ");
		int id = input.nextInt();
		System.out.print("Enter the road name: ");
		input.nextLine();
		String road = input.nextLine();
		System.out.print("Enter the position: ");
		double position = input.nextDouble();
		System.out.print("Enter the time: ");
		double time = input.nextDouble();
		
		try {
			monitor.addData(id, road, position, time);
			double speed = monitor.getCars(id).getSpeed();
			System.out.println("\nAdded car #" + id + " on " + road + ", " +
				"Position = " + position + ", Time = " + time
				+ (speed > -1 ? "Speed = " + speed : "")); // only print the speed if this car has one
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * Remove the given car from its respective road
	 */
	public static void removeCar() {
		System.out.print("Enter the car number: ");
		int id = input.nextInt();
		
		try {
			monitor.remove(id);
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());			
		}
		System.out.println();
	}
	
	/**
	 * Return the name of the road that the given car is currently on
	 */
	public static void getRoad() {
		System.out.print("Enter the car ID number: ");
		int id = input.nextInt();
		
		String road = monitor.getRoad(id);
		if (road == null) {
			System.out.println("\nCar #" + id + " is unknown");
			return;
		}
		
		System.out.println("\nCar #" + id + " is on " + monitor.getRoad(id));
	}
	
	/**
	 * Return the current speed of the given car 
	 */
	public static void getASpeed() {
		System.out.print("Enter the car number: ");
		int id = input.nextInt();
		double speed = monitor.getSpeed(id);
		
		if (speed == -1) {
			System.out.println("\nCar #" + id + " is unknown.");
			return;
		}		
		if (speed == -2) {
			System.out.println("\nCar #" + id + "'s speed is unknown.");
			return;
		}
		
		System.out.println("\nThe speed of car #" + id + " is " + speed);
	}
	
	/**
	 * Return all of the car speeds on the given road
	 * and display the car ID along with the speed
	 */
	public static void getAllSpeeds() {
		System.out.print("Enter the street: ");
		input.nextLine();
		String road = input.nextLine();
		System.out.println();
		
		try {
			List<Double> allSpeeds = monitor.getKnownSpeeds(road);
			int index = 0;
			for (double d: allSpeeds) {
				System.out.print("Car #" + monitor.knownId.get(index) + ": ");
				System.out.println(d);
				index++;
			}
		}
		catch (IllegalArgumentException e) {
			System.out.println("\n" + e.getMessage() + ".");
		}				
	}
	
	/**
	 * Return the average speed of all the cars on the given road
	 */
	public static void getAvgSpeed() {
		System.out.print("Enter the street: ");
		input.nextLine();
		String road = input.nextLine();
		System.out.println();
		
		try {
			double avgSpeed = monitor.getAverageSpeed(road);
			if (avgSpeed == -1) {
				System.out.println("Cannot get the average speed on a road with no known speeds.");
				return;
			}
			System.out.println("Average speed of cars on " + road + " is " + avgSpeed);			
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}		
	}
	
	/** Quits the program and saves the current monitor to a .obj file */
	public static void quitAndSave() {
		try {
			monitor.serialize(filename);
			System.out.println("Saved monitor to \"" + filename + "\" Click OK to close program.");
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
		
		System.exit(0);
	}
}
