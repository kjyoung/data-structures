/*
 * 
 * Kevin Young
 * 109561787
 * Homework 7
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.util.Comparator;

/**
 * Compares two Employers based by name.
 * @author Kevin
 *
 */
public class EmployerNameComparator implements Comparator<Employer> {
	
	/**
	 * Return -1 if first comes alphabetically before second, 
	 * zero if they have the same name, or +1 otherwise. 
	 * Ignore case for this comparison.
	 */
	public int compare(Employer first, Employer second) {
		int compare = first.getName().compareToIgnoreCase(second.getName());
		
		if (compare < 0) {
			return -1;			
		} else if (compare == 0) {
			return 0;
		} else {
			return 1;
		}
	}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

}
