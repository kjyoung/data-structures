/*
 * 
 * Kevin Young
 * 109561787
 * Homework 7
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.util.Comparator;

/**
 * Compare JobPostings by salary.
 * @author Kevin
 *
 */
public class JobPostingSalaryComparator implements Comparator<JobPosting> {
	
	/**
	 * Return -1 if first has a lower salary than second, 
	 * zero if they have the same salary, 
	 * or a positive number otherwise. 
	 */
	public int compare(JobPosting first, JobPosting second) {
		if (first.getSalary() < second.getSalary()) {
			return -1;
		} else if (first.getSalary() == second.getSalary()) {
			return 0;
		} else {
			return 1;
		}
	}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

}