/*
 * 
 * Kevin Young
 * 109561787
 * Homework 7
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.util.Comparator;

/**
 * Compare JobPostings by number of vacation days per year.
 * @author Kevin
 *
 */
public class JobPostingVacationComparator implements Comparator<JobPosting> {
	
	/**
	 * Return -1 if first has fewer vacation days than second, 
	 * zero if they have the same number of vacation days, 
	 * or +1 otherwise. 
	 */
	public int compare(JobPosting first, JobPosting second) {
		if (first.getVacDays() < second.getVacDays()) {
			return -1;
		} else if (first.getVacDays() == second.getVacDays()) {
			return 0;
		} else {
			return 1;
		}
	}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

}
