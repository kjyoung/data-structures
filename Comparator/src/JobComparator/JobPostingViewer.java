/*
 * 
 * Kevin Young
 * 109561787
 * Homework 7
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

@SuppressWarnings("unchecked")
public class JobPostingViewer {
	
	private static ArrayList empList = new Employer(); // holds all the Employers. This list will be manipulated throughout the run
	private static String fileName = "employers.obj"; // at the start of the program will it attempt to load an object file with the name of this variable
	private static Scanner input = new Scanner(System.in);	
	private static boolean runAgain = true; // makes sure the menu keeps repeating until the program is terminated by the user
	
	/* If the consecutive sort is the same as the sort key, then the list is sorted */
	private static char empSortKey; // the last means of sorting for the list of Employers.
	private static char jobSortKey; // the last means of sorting for the list of JobPostings
	private static boolean jobsReversed = false; // true if the list of Employers has been reversed 
	private static boolean empsReversed = false; // true if the list of JobPostings has been reversed
	
	/** Comparators to sort the job listings */
	private static JobPostingNameComparator sortName = new JobPostingNameComparator();
	private static JobPostingSalaryComparator sortSalary = new JobPostingSalaryComparator();
	private static JobPostingHoursComparator sortHours = new JobPostingHoursComparator();
	private static JobPostingVacationComparator sortVacation = new JobPostingVacationComparator();		
	private static Comparator<JobPosting> reverseName = Collections.reverseOrder(sortName);
	private static Comparator<JobPosting> reverseSalary = Collections.reverseOrder(sortSalary);
	private static Comparator<JobPosting> reverseHours = Collections.reverseOrder(sortHours);
	private static Comparator<JobPosting> reverseVacation = Collections.reverseOrder(sortVacation);
	
	/** Comparators to sort the employers */
	private static EmployerNameComparator sortEmpName = new EmployerNameComparator();
	private static EmployerNumJobsComparator sortEmpJobs = new EmployerNumJobsComparator();	
	private static Comparator<Employer> reverseEmpName = Collections.reverseOrder(sortEmpName);  
	private static Comparator<Employer> reverseEmpJobs = Collections.reverseOrder(sortEmpJobs);
	
	private static int salaryWeight, hoursWeight, vacationWeight; // used for the JobPostingWeightedComparable object
	
	public static void main(String[] args) {
		loadEmployerFromFile();
		while (runAgain) {
			printMenu();
		}
	}
	
	/** 
	 * Gets an Employer object file and sets it as the Employer, if the Employer exists. 
	 */
	public static void loadEmployerFromFile() {
		ArrayList<Employer> newEmpList;
		try {
			newEmpList = Employer.deserialize(fileName);
			if (newEmpList != null) {
				System.out.println("Loaded Employer from \"" + fileName + "\"\n");
				empList = newEmpList;
			} else
				System.out.println("\"" + fileName + "\" not found or not an Employer type. Using new Employer type.\n");		
		} catch (ClassNotFoundException e) {
			System.out.println("\"" + fileName + "\" not a Employer type. Using a new Employer type\n");
		} catch (IOException e) {
			System.out.println("\"" + fileName + "\" not found\n");
		}		
	}
	
	/** 
	 * Prints a menu which the user will choose from to perform an operation 
	 */
	public static void printMenu() {		
		System.out.println("MENU: \n\tA) Add Employer\n" +
				"\tB) Add Job\n" +
				"\tC) Remove Employer\n" +
				"\tD) Remove Job\n" +
				"\tE) Sort and print Employers\n" +
				"\tF) Sort and print Jobs\n" +
				"\tQ) Quit and save to \"" + fileName + "\"\n");
		System.out.print("Enter a selection: ");
		String selection = input.next().toUpperCase();
		
		switch (selection) {
			case "A": System.out.println();
					  addEmployer();
					  System.out.println();
					  break;
			case "B": System.out.println();
					  addJob();
					  System.out.println();
					  break;
			case "C": System.out.println();
					  removeEmp();
					  System.out.println();
					  break;
			case "D": System.out.println();
					  removeJob();
					  System.out.println();
					  break;
			case "E": System.out.println();
					  sortAndPrintEmps();
					  System.out.println();
					  break;
			case "F": System.out.println();
					  sortAndPrintJobs();
					  System.out.println();
					  break;
			case "Q": System.out.println();
					  quitAndSave();
					  System.out.println();
					  break;
		}		
	}
	
	/** 
	 * Adds a new employer to the list
	 */
	public static void addEmployer() {
		System.out.print("Enter the employer's name: ");
		input.nextLine();
		String name = input.nextLine();
		
		empList.add(new Employer(name));
		
		sortEmps(empSortKey);
		
		int i = -1;
		while (++i < empList.size()) {
			Employer emp = (Employer)empList.get(i);
			System.out.print((i+1) + "  ");
			System.out.printf("%-34s%-10d\n", emp.getName(), emp.size());
		}
	}
	
	/** 
	 * Sorts the employers based on the names and number of jobs of the employer
	 * and prints the sorted list
	 */
	public static void sortAndPrintEmps() {
		
		/** Print the menu */
		System.out.println("A) Sort by name \n" +
				           "B) Sort by number of jobs");		
		System.out.print("\nEnter a selection: ");	
		
		/* Sort the employers listings and update the employer sort key */
		char key = input.next().toUpperCase().charAt(0);
		sortEmps(key);
		empSortKey = key;		
		
		/** Print the table */
		System.out.println("\n#  Employer			     # Jobs");
		System.out.println("-- --------                          ------");
		int i = -1;
		while (++i < empList.size()) {
			Employer emp = (Employer)empList.get(i);
			System.out.print((i+1) + "  ");
			System.out.printf("%-34s%-10d\n", emp.getName(), emp.size());
		}
	}
	
	/**
	 * Adds a job to the given employer
	 */
	public static void addJob() {
		System.out.print("Enter the number of the employer: ");
		int index = input.nextInt();
		index--;
		
		/** Validate this employer exists */
		if (!employerExists(index)) {
			return;
		}
		
		/* Get user input */
		System.out.print("Enter the job title: ");
		input.nextLine();
		String title = input.nextLine();
		System.out.print("Enter the salary: ");
		int salary = input.nextInt();
		System.out.print("Enter the number of hours per week: ");
		int hours = input.nextInt();
		System.out.print("Enter the number of vacation days per year: ");
		int vacDays = input.nextInt();
		
		/** Add the job to the list based on the previous sort key */
		((ArrayList<JobPosting>) empList.get(index)).add(new JobPosting(title, salary, hours, vacDays));		
		
		sortJobs(index, jobSortKey, false);		
		
		System.out.println("\nAdded \"" + title + "\" to " + ((Employer) empList.get(index)).getName());
		
	}
	
	/** 
	 * Sorts the job listings based on the names, salaries, hours, vacation days, or preference score,
	 * and prints the sorted list
	 */
	public static void sortAndPrintJobs() {
		System.out.print("Enter the employer number: ");
		int index = input.nextInt();
		index--;
		
		/** Validate this employer exists */
		if (!employerExists(index)) {
			return;
		}
		
		/** Print the menu */
		System.out.println("\nA) Sort by name\nB) Sort by salary\nC) Sort by hours per week\n" +
				"D) Sort by vacation days per year\nE) Sort by custom preference score");		
		System.out.print("\nEnter a selection: ");
		
		/* Sort the job listings and update the job sort key */
		char key = input.next().toUpperCase().charAt(0);
		sortJobs(index, key, true);
		jobSortKey = key;
		
		/** Print the table */
		System.out.println("\n#   Job Title\t\t\t      Salary   Hours   Vacation");
		System.out.println("--  ---------\t\t\t      ------   -----   --------");				
		int i = -1;
		while (++i < ((ArrayList<?>) empList.get(index)).size()) {
			ArrayList<JobPosting> emp = ((ArrayList<JobPosting>) empList.get(index));
			System.out.print((i+1) + "   ");
			System.out.printf("%-34s%-9d%-8d%-10d\n", emp.get(i).getTitle(), emp.get(i).getSalary(), emp.get(i).getHours(), emp.get(i).getVacDays());
		}
		
	}
	
	/** 
	 * Removes the given employer from the list
	 */
	public static void removeEmp() {
		System.out.print("Enter the number of the employer to remove: ");		
		int index = input.nextInt();
		index--;		

		/** Validate this employer exists */
		if (!employerExists(index)) {
			return;
		}
		
		System.out.print("\nRemoved employer \"" + ((Employer) empList.get(index)).getName() + "\"");
		empList.remove(index);
	}
	
	/** 
	 * Removes the given job from the given employer 
	 */
	public static void removeJob() {
		System.out.print("Enter the number of the employer: ");
		int empIndex = input.nextInt();
		empIndex--;		

		/** Validate this employer exists */
		if (!employerExists(empIndex)) {
			return;
		}
		
		System.out.print("Enter the number of the job to remove: ");
		int jobIndex = input.nextInt();
		jobIndex--;
		
		/** Validate this job exists */
		if (!jobExists(empIndex, jobIndex)) {
			return;
		}
		
		System.out.println("\nRemoved job \"" + ((JobPosting) ((ArrayList<?>) empList.get(empIndex)).get(jobIndex)).getTitle() +
				"\" from employer \"" + ((Employer) empList.get(empIndex)).getName() + "\"");
		((ArrayList<?>) empList.get(empIndex)).remove(jobIndex);
	}
	
	
	/** 
	 * Quits the program and saves the current Employer to a .obj file 
	 */
	public static void quitAndSave() {
		try {
			((Employer) empList).serialize(fileName);
			System.out.println("Saved Employer to \"" + fileName + "\"");
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
		
		System.exit(0);
	}
	
	/** 
	 * Validates the employer exists 
	 * @param index The employer to validate
	 * @return true if the employer exists, else false
	 */
	public static boolean employerExists(int index) {
		try {
			empList.get(index);
			return true;
		} catch (IndexOutOfBoundsException e) {
			System.out.println("This employer is not listed.");
			return false;
		}		
	}
	
	/**
	 * Validates the job of the given employer exists
	 * @param empIndex The employer who may or may not hold this job
	 * @param jobIndex The job to validate
	 * @return true if the job exists, else false
	 */
	public static boolean jobExists(int empIndex, int jobIndex) {
		try {
			((ArrayList<?>) empList.get(empIndex)).get(jobIndex);
			return true;
		} catch (IndexOutOfBoundsException e) {
			System.out.println("This job is not listed.");
			return false;
		}
	}
	
	/**
	 * Inserts this new employer based on the given key
	 * @param key If key = 'A' sort by Employer name
	 * 			  If key = 'B' sort by number of job listings
	 */
	public static void sortEmps(char key) {
		if (key == empSortKey && !empsReversed) {
			switch (key) {
				case 'A' : Collections.sort(empList, reverseEmpName); break;
				case 'B' : Collections.sort(empList, reverseEmpJobs); break;
			}
			empsReversed = true;
		} else {
			switch (key) {
				case 'A' : Collections.sort(empList, sortEmpName); break;
				case 'B' : Collections.sort(empList, sortEmpJobs); break;
			}
			empsReversed = false;
		}
	}
	
	/**
	 * Inserts this new job listing to the given employer, based on the given key, to the given employer
	 * @param key If key = 'A' sort based on name
	 *            If key = 'B' sort based on salary
	 *            If key = 'C' sort based on hours
	 *            If key = 'D' sort based on vacation days
	 *            If key = 'E' sort based on preference score
	 */
	public static void sortJobs(int index, char key, boolean printMenu) {		
		
		/*  If the user attempts to sort a list by the previous sort key used, reverse the ordering */
		if (key == jobSortKey && !jobsReversed) {
			switch (key) {
				case 'A' : Collections.sort(((ArrayList<JobPosting>) empList.get(index)), reverseName); break;
				case 'B' : Collections.sort(((ArrayList<JobPosting>) empList.get(index)), reverseSalary); break;
				case 'C' : Collections.sort(((ArrayList<JobPosting>) empList.get(index)), reverseHours); break;
				case 'D' : Collections.sort(((ArrayList<JobPosting>) empList.get(index)), reverseVacation); break;
				case 'E' : sortByPrefScore(index, key, printMenu); break;
			}
			jobsReversed = true;
		} else {
			switch (key) {
				case 'A' : Collections.sort(((ArrayList<JobPosting>) empList.get(index)), sortName); break;
				case 'B' : Collections.sort(((ArrayList<JobPosting>) empList.get(index)), sortSalary); break;
				case 'C' : Collections.sort(((ArrayList<JobPosting>) empList.get(index)), sortHours); break;
				case 'D' : Collections.sort(((ArrayList<JobPosting>) empList.get(index)), sortVacation); break;
				case 'E' : sortByPrefScore(index, key, printMenu); break;
			}	
			jobsReversed = false;
		}
	}
	
	/** 
	 * Sort the job listing based on a user-defined preference score 
	 */
	public static void sortByPrefScore(int index, char key, boolean printMenu) {
		if (printMenu) {
			System.out.print("\nEnter the salary weight: ");
			salaryWeight = input.nextInt();
			System.out.print("Enter the hours/week weight: ");
			hoursWeight = input.nextInt();
			System.out.print("Enter the vacation days/year weight: ");
			vacationWeight = input.nextInt();
		}
		
		JobPostingWeightedComparator sortPrefScore = new JobPostingWeightedComparator(salaryWeight, hoursWeight, vacationWeight);
		
		if (key == jobSortKey) {
			Collections.sort(((ArrayList<JobPosting>) empList.get(index)), Collections.reverseOrder(sortPrefScore));	
		} else {
			Collections.sort(((ArrayList<JobPosting>) empList.get(index)), sortPrefScore);
		}				
	}
}
