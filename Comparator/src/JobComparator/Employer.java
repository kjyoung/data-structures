/*
 * 
 * Kevin Young
 * 109561787
 * Homework 7
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class represents a list of one employer's job postings.
 * @author Kevin
 *
 */
public class Employer extends ArrayList<JobPosting> implements Serializable {
	
	private String name; // the name of the employer
	
	public Employer() {}
	
	/**
	 * Creates a new Employer object with the specified name
	 * @param name
	 */
	public Employer(String name) {
		this.name = name;
	}
	
	/**
	 * Returns the name of this employer
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name of this employer as the given string
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}	
	
	/**
	  * Opens a FileOutputStream and an ObjectOutputStream, and uses the 
	  * ObjectOutputStream to write the Employer to the specified binary
	  * file, then the method closes the two output streams.
	  * @param filename the binary file to write the Employer's content to
	  * @throws IOException 
	  */
	 public void serialize(String fileName) {
		 ObjectOutputStream outStream = null;
		 try {
			 FileOutputStream file = new FileOutputStream(fileName);
			 outStream = new ObjectOutputStream(file);
			 outStream.writeObject(this); // Writes this object to "fileName".obj
			 outStream.close();
			 file.close();
		 }
		 catch (IOException e) {
			 System.out.println("Unable to write this object to the file \"" + fileName + "\"");
		 }
	 }
	 
	 /**
	  * Opens a FileInputStream and an ObjectInputStream, and uses the 
	  * ObjectInputStream to read an Employer from the specified binary
	  * file, then closes the two output streams and returns the tree. 
	  * Returns null if the file was not found, or did not contain a valid 
	  * Employee object.
	  * @param filename the binary file where the Employer's data will be read from
	  * <dt><b>Precondition:</b><dd>
	  *  The file was found, and if it does, the file contains a valid Employer object
	  * @return The Employee in the ObjectInputStream
	  * @throws IOException 
	  * @throws ClassNotFoundException 
	  */
	public static ArrayList<Employer> deserialize(String filename) throws ClassNotFoundException, IOException {
		 ArrayList<Employer> emp;
		 try {
		      FileInputStream file = new FileInputStream(filename);
		      ObjectInputStream inStream  = new ObjectInputStream(file);
		      emp = (ArrayList<Employer>) inStream.readObject(); 
		      inStream.close();
		      file.close();
		 } catch (FileNotFoundException e) {
			 return null;
		 } catch (ClassNotFoundException e) {
			 return null;
		 } catch (ClassCastException e) { 
			 return null;
		 } catch (IOException e) {
			 return null;
		 }
		 return emp;
	 }
	
}