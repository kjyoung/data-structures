/*
 * 
 * Kevin Young
 * 109561787
 * Homework 7
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.util.Comparator;

/**
 * Compare JobPostings by a user defined weighted sum of salary, hours per day, and vacation days per year.
 * @author Kevin
 *
 */
public class JobPostingWeightedComparator implements Comparator<JobPosting> {
	
	private int salaryWeight;
	private int hoursWeight;
	private int vacationWeight;

	public JobPostingWeightedComparator(int salaryWeight, int hoursWeight, int vacationWeight) {
		this.salaryWeight = salaryWeight;
		this.hoursWeight = hoursWeight;
		this.vacationWeight = vacationWeight;
	}
	
	/**
	 * Return -1 if the preference score of the first JobPosting is less than the second 
	 * else if they're equal return 0
	 * otherwise return 1
	 * Note that since larger salary and vacation days are more preferable, their weights will generally be positive.
	 * But since more hours per week is less preferable, hoursWeight will generally be negative. 
	 */
	public int compare(JobPosting first, JobPosting second) {
		int firstPrefScore = getPrefScore(first);
		int secondPrefScore = getPrefScore(second);
		
		if (firstPrefScore < secondPrefScore) {
			return -1;
		} else if (firstPrefScore == secondPrefScore) {
			return 0;
		} else {
			return 1;
		}		
	}
	
	/**
	 * Preference score is calculated by 
	 * preference = salary * salaryWeight + hours * hoursWeight + vacationDays * vacationWeight.
	 * @param jp The JobPosting to get the score of
	 * @return The preference score for this JobPosting
	 */
	public int getPrefScore(JobPosting jp) {
		return jp.getSalary() * salaryWeight + jp.getHours() * hoursWeight + jp.getVacDays() * vacationWeight;
	}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JobPostingWeightedComparator comparator2 = (JobPostingWeightedComparator) obj;
		if (hoursWeight != comparator2.hoursWeight)
			return false;
		if (salaryWeight != comparator2.salaryWeight)
			return false;
		if (vacationWeight != comparator2.vacationWeight)
			return false;
		return true;
	}

}
