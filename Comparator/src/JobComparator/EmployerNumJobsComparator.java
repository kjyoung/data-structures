/*
 * 
 * Kevin Young
 * 109561787
 * Homework 7
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.util.Comparator;

/**
 * Compare by the number of job postings each Employer has
 * @author Kevin
 *
 */
public class EmployerNumJobsComparator implements Comparator<Employer> {
	
	/** 
	 *  Return -1 if first has fewer postings than second, zero if they have 
	 *  the same number of postings, or a positive number otherwise.
	 */
	public int compare(Employer first, Employer second) {
		int firstCount = getCount(first);
		int secondCount = getCount(second);
		
		if (firstCount < secondCount) {
			return -1;
		} else if (firstCount == secondCount) {
			return 0;
		} else {
			return 1;
		}
	}
	
	/**
	 * Returns the number of job postings this employer has
	 * @param employer
	 * @return The size of this Employer type
	 */
	public int getCount(Employer employer) {
		int count = 0;
		while (count++ < employer.size()) {}
		return count;
	}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
	
}
