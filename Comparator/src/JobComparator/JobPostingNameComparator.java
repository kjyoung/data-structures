/*
 * 
 * Kevin Young
 * 109561787
 * Homework 7
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.util.Comparator;

/**
 * Compare JobPostings by name.
 * @author Kevin
 *
 */
public class JobPostingNameComparator implements Comparator<JobPosting> {
	
	/**
	 * Return -1 if first comes alphabetically before second, 
	 * zero if they have the same name, 
	 * or a positive number otherwise. 
	 * Case-insensitive comparison.
	 */
	public int compare(JobPosting first, JobPosting second) {
		int compare = first.getTitle().compareToIgnoreCase(second.getTitle());
		
		if (compare < 0) { // o1 is less than o2
			return -1;			
		} else if (compare == 0) {
			return 0;
		} else {
			return 1;
		}
	}
	
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}
	
}
