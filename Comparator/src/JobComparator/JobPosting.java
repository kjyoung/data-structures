/*
 * 
 * Kevin Young
 * 109561787
 * Homework 7
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 * 
 */

import java.io.Serializable;

/**
 * This class represents one job posting.
 * @author Kevin
 *
 */
public class JobPosting implements Serializable {
	
	private String title; // the name of the job (ex. janitor)
	private int salary; // how much pay this job offers in a year
	private int hours; // how many hours a week this job requires
	private int vacDays; // how many vacation days this job allows in a year
	
	/**
	 * Creates a new JobPosting object with all instance variables set to user
	 * values
	 * @param title
	 * @param salary
	 * @param hours
	 * @param vacDays
	 */
	public JobPosting(String title, int salary, int hours, int vacDays) {
		this.title = title;
		this.salary = salary;
		this.hours = hours;
		this.vacDays = vacDays;
	}
	
	/**
	 * Returns the title for this JobPosting
	 * @return
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Changes the title for this JobPosting
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/** 
	 * Returns the salary of this JobPosting
	 * @return
	 */
	public int getSalary() {
		return salary;
	}
	
	/**
	 * Changes the salary of this JobPosting
	 * @param salary
	 */
	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	/**
	 * Returns the number of required working hours for this
	 * JobPosting
	 * @return
	 */
	public int getHours() {
		return hours;
	}
	
	/**
	 * Changes the number of required working hours for
	 * this JobPosting
	 * @param hours
	 */
	public void setHours(int hours) {
		this.hours = hours;
	}
	
	/**
	 * Returns the number of vacation days this job offers
	 * per year
	 * @return
	 */
	public int getVacDays() {
		return vacDays;
	}
	
	/**
	 * Changes the number of vacation days this job offers
	 * per year
	 * @param vacDays
	 */
	public void setVacDays(int vacDays) {
		this.vacDays = vacDays;
	}
	
}