/** 
 * Kevin Young
 * 109561787
 * Homework 2
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 */

/**
 * This class contains a reference to an AirportStatus object as well as 
 * to two other AirportNode objects, referred to as prev and next
 */
public class AirportNode extends AirportStatus {

	private AirportStatus data; 
	private AirportNode prev; /* the node before the current node */
	private AirportNode next; /* the node after the current node */
	
	/**
	 * Constructor for an empty node
	 */
	public AirportNode() {	
		next = null;
		prev = null;
		data = null;
	}
	
	/**
	 * Constructor for a node containing the given airport
	 * @param data the airport to be stored
	 */
	public AirportNode(AirportStatus data) {
		this.data = data;	
		next = null;
	}
	
	/**
	 * Changes the airport stored in the current node
	 * @param data the airport to be stored
	 */
	public void setData(AirportStatus data) {
		this.data = data;
	}
	
	/**
	 * Returns the airport data in the current node
	 * @return the airport object
	 */
	public AirportStatus getData() {
		if (data == null)
			return null;
		else
			return data;
	}
	
	/**
	 * Change the airport stored in the node after the current node
	 * @param next the airport to be stored in the next node
	 */
	public void setNext(AirportNode next) {
		this.next = next;
	}
	
	/**
	 * Change the airport stored in the node before the current node
	 * @param prev the airport to be stored in the previous node
	 */
	public void setPrev(AirportNode prev) {
		this.prev = prev;
	}
	
	/**
	 * Returns the airport storred in the node after the current node
	 * @return the airport stored in the next node
	 */
	public AirportNode getNext() {
		if (next == null) 
			return null;
		else
			return next;
	}
	
	/**
	 * Returns the airport storred in the node before the current node
	 * @return the airport stored in the previous node
	 */
	public AirportNode getPrev() {
		if (prev == null)
			return null;
		else
			return prev;
	}
	
	/**
	 * Header for the table which displays the data of the airport in the current node 
	 */
	public void displayTableHeader() {
		data.displayTableHeader();
	}
	
	/**
	 * Body for the table which displays the data of the airport in the current node 
	 */
	public void  displayTableBody() {
		data.displayTableBody();
	}
}
