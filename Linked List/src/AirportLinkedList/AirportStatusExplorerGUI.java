/** 
 * Kevin Young
 * 109561787
 * Homework 2
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 */

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;


public class AirportStatusExplorerGUI extends JFrame {
	
	JPanel panel1 = new JPanel();
	JPanel panel2 = new JPanel();
	JButton a = new JButton("Add to end");
	JButton r = new JButton("Remove current node");
	JButton b = new JButton("Move current node backward");
	JButton j = new JButton("Jump to position");
	JButton d = new JButton("Display all nodes");
	JButton c = new JButton("Display current node");
	JButton i = new JButton("Insert after current node");
	JButton f = new JButton("Move current node forward");
	JButton q = new JButton("Exit");
	Font font = new Font("Verdana", Font.BOLD, 20);
	static String stringInput; /* this will be used to take input entered into JOptionPane */
	static AirportList list = new AirportList();
	private final static JTextArea field = new JTextArea();	/* this will display all messages to the user */
	
	public AirportStatusExplorerGUI() {
		
		getContentPane().setBackground(Color.DARK_GRAY);
		setTitle("Menu");
		
		/* GridBagLayout for panel1 */
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{66, 705, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{67, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		GridBagConstraints gbc_panel1 = new GridBagConstraints();
		gbc_panel1.gridwidth = 4;
		gbc_panel1.insets = new Insets(0, 0, 5, 5);
		gbc_panel1.anchor = GridBagConstraints.SOUTH;
		gbc_panel1.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel1.gridx = 0;
		gbc_panel1.gridy = 0;
		getContentPane().add(panel1, gbc_panel1);
		
		/* Adding buttons and editing of the first JPanel */
		panel1.setBackground(Color.DARK_GRAY);
		panel1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));	
		a.setFont(new Font("Narkisim", Font.PLAIN, 20));
		a.setBackground(Color.CYAN);
		a.setVerticalAlignment(SwingConstants.TOP);
		panel1.add(a);
		i.setBackground(Color.CYAN);
		i.setFont(new Font("Narkisim", Font.PLAIN, 20));
		panel1.add(i);
		f.setBackground(Color.CYAN);
		f.setFont(new Font("Narkisim", Font.PLAIN, 20));
		panel1.add(f);
		b.setBackground(Color.CYAN);
		b.setFont(new Font("Narkisim", Font.PLAIN, 20));
		panel1.add(b);
		q.setForeground(Color.BLACK);
		q.setBackground(Color.CYAN);
		q.setFont(new Font("Narkisim", Font.BOLD, 20));
		panel1.add(q);
		panel1.setBackground(Color.DARK_GRAY);
		panel1.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));	
		
		/* Adding buttons and editing of the second JPanel */
		panel2.setBackground(Color.DARK_GRAY);
		GridBagConstraints gbc_panel2 = new GridBagConstraints();
		gbc_panel2.gridwidth = 2;
		gbc_panel2.fill = GridBagConstraints.HORIZONTAL;
		gbc_panel2.insets = new Insets(0, 0, 5, 5);
		gbc_panel2.anchor = GridBagConstraints.NORTH;
		gbc_panel2.gridx = 0;
		gbc_panel2.gridy = 1;
		getContentPane().add(panel2, gbc_panel2);
		r.setBackground(Color.CYAN);
		r.setVerticalAlignment(SwingConstants.TOP);
		panel2.add(r);
		r.setFont(new Font("Narkisim", Font.PLAIN, 20));
		d.setBackground(Color.CYAN);
		d.setFont(new Font("Narkisim", Font.PLAIN, 20));
		panel2.add(d);
		c.setBackground(Color.CYAN);
		c.setFont(new Font("Narkisim", Font.PLAIN, 20));
		panel2.add(c);
		/* Adding ActionListeners */
		a.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)  {
				performOperation("a");
			}
		});
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)  {
					performOperation("b");				
			}
		});
		q.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)  {
				performOperation("q");
			}
				
		});
		f.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)  {
					performOperation("f");
			}
		});
		i.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)  {
				performOperation("i");
			}
		});
		c.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)  {
				performOperation("c");
			}
		});
		r.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)  {
				performOperation("r");
			}
		});
		d.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)  {
				performOperation("d");
			}
		});
		q.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)  {
					performOperation("q");
			}
		});
		
		/* Adding and editing of the JTextArea field */
		GridBagConstraints gbc_textArea = new GridBagConstraints();
		gbc_textArea.anchor = GridBagConstraints.NORTH;
		gbc_textArea.insets = new Insets(21, 0, 5, 5);
		gbc_textArea.gridx = 1;
		gbc_textArea.gridy = 2;
		field.setWrapStyleWord(true);
		field.setEditable(false);
		field.setFont(new Font("Monospaced", Font.BOLD, 11));
		field.setBackground(Color.DARK_GRAY);
		field.setForeground(Color.WHITE);
		getContentPane().add(field, gbc_textArea);	
		
		this.setSize(1920,600);
		setVisible(true);		
	}		
	
	public static void main(String[] args) {
		AirportStatusExplorerGUI newGui = new AirportStatusExplorerGUI();
	}
	
	public static void performOperation(String prompt) {
	
			switch (prompt.toUpperCase()) {
				case ("A") :
					addToEnd();
					break;
				case ("D") :
					displayList();
					break;
				case ("I") :
					insertAfterCurrent();
					break;
				case ("C") :
					displayAirport();
					break;
				case ("R") :
					removeNode();
					break;
				case ("F") :
					moveForward();
					break;
				case ("B") :
					moveBackward();
					break;
				case ("J") :
					jumpToPosition();
					break;
				case ("Q") :
					System.out.print("Exiting program..");
					System.exit(0);		
					break;
				default :
					System.out.println("Invalid operation");
					break;
			}	
	}	

	/**
	 * Asks for a three letter airport code, and stores this in the last node
	 * of the list
	 */
	public static void addToEnd() {	
		stringInput = JOptionPane.showInputDialog("Enter the airport code");
		AirportStatus newAir = new AirportStatus(stringInput.toUpperCase());
		newAir.setAllData();
		list.addToEnd(newAir);
		field.setText("Added:\n\n" + newAir.toString());
	}

	/**
	 * Displays the data for all the airports stored in nodes in the list 
	 */
	public static void displayList() {
		field.setText(list.toString());
	}

	/**
	 * Prompts the user for an airport code, creates an AirportStatus 
	 * object for this airport, and adds it after the current node.
	 * If there is no current node, the new node is inserted at the
	 * end of the list.
	 */
	public static void insertAfterCurrent() {
		stringInput = JOptionPane.showInputDialog("Enter the airport code");	
		AirportStatus newAir = new AirportStatus(stringInput.toUpperCase());
		newAir.setAllData();
		list.addAfterCurrent(newAir);
		field.setText("Inserted:\n\n " + newAir.toString());	
	}

	/**
	 * Displays only the data stored in the current node
	 */
	public static void displayAirport() {
		try {
			field.setText(list.getCursor().getData().toString());
		} catch (NullCursorException e) {
			field.setText(e.getMessage());
		}
	}

	/** 
	 * If there is a current node, it is removed from the list 
	 * and the current node is moved to the next appropriate current node. 
	 * If there is no current node, user is informed of the situation 
	 * with an error message
	 */
	public static void removeNode() {
		String tempAirportCode = "";
		if (!(list.isEmpty())) {
			try {
				tempAirportCode = list.getCursor().getData().getAirportCode();
			} catch (NullCursorException e) {
				field.setText(e.getMessage());
			}
		}
		if (list.removeCurrentNode() == true) {
			field.setText("Removed " + tempAirportCode + "\n");
		}
		else {
			field.setText("This node does not exist\n");
		}
	}

	/**
	 * Moves the current node forward by one position if there is another
	 * node in the list. If the current slide is the last in the list,
	 * an appropriate message is displayed to the user and nothing is done
	 */
	public static void moveForward() {
		try {
			if (list.moveForward()) {
				field.setText("The current node has successfully been moved " +
					            "forward one position");
			}
			else {
				field.setText("There is no next node. Node has not been moved");
			}
		} catch (NullCursorException e) {
			field.setText(e.getMessage());
		}
	}	

	/**
	 * Moves the current node backward by one position if there is a previous
	 * node in the list. If the current slide is the first in the list,
	 * an appropriate message is displayed to the user and nothing is done
	 */
	public static void moveBackward() {
		try {
			if (list.moveBack()) {
				field.setText("The current node has successfully been moved " +
								"backward one position");
			}
			else {
				field.setText("There is no previous node. Node has not been moved");
			}
		} catch (NullCursorException e) {
			field.setText(e.getMessage());
		}
	}

	/**
	 * Prompts the user for a position number and moves the current node 
	 * to that number node, if it exists. If it does not exist, simply 
	 * an error message is displayed to the user and nothing is done
	 */
	public static void jumpToPosition() {
		int position;
		stringInput = JOptionPane.showInputDialog("Enter the position");
		position = Integer.parseInt(stringInput);
		if (list.jumpToPosition(position)) {
			field.setText("The current node has successfully been moved to " +
				"position " + position + "\n");
		}
		else {
			field.setText("This node does not exist. Node has not been moved\n");
		}
	}
}