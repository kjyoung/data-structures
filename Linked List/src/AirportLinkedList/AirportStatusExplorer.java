/** 
 * Kevin Young
 * 109561787
 * Homework 2
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 */

import java.util.Scanner;

/**
 * This class contains a main method that presents a menu that
 *  allows the user to access data from the API, store it in a 
 *  linked list, and interact with it using commands *
 */
public class AirportStatusExplorer extends AirportStatus {

	static AirportList list = new AirportList();
	static String prompt; /* user given instruction which will select an operation */
	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		do {
			System.out.println("A) Add to end\nI) Insert after current node\n" +
					"R) Remove current node\nC) Display current node\n" +
					"D) Display all nodes\nF) Move current node forward\n" +
					"B) Move current node backward\nJ) Jump to position\nQ) Exit");
			System.out.print("\nChoose an operation: ");
			prompt = input.next().toUpperCase();
			switch (prompt) {
				case ("A") :
					addToEnd();
					break;
				case ("D") :
					displayList();
					break;
				case ("I") :
					insertAfterCurrent();
					break;
				case ("C") :
					displayAirport();
					break;
				case ("R") :
					removeNode();
					break;
				case ("F") :
					moveForward();
					break;
				case ("B") :
					moveBackward();
					break;
				case ("J") :
					jumpToPosition();
					break;
				case ("Q") :
					System.out.print("Exiting program..");
					System.exit(0);		
					break;
				default :
					System.out.println("Invalid operation");
					break;
			}
		}
		while (!(prompt.equals("Q")));			
	}
	
	/**
	 * Asks for a three letter airport code, and stores this in the last node
	 * of the list
	 */
	public static void addToEnd() {		
		System.out.print("\nEnter the airport code: ");
		AirportStatus newAir = new AirportStatus(input.next().toUpperCase());
		newAir.setAllData();
		list.addToEnd(newAir);
		System.out.println("\nAdded:\n");
		newAir.displayTableHeader();
		newAir.displayTableBody();
		System.out.println("\n");
	}
	
	/**
	 * Displays the data for all the airports stored in nodes in the list 
	 */
	public static void displayList() {
		System.out.println();
		try {
			list.displayCurrentList();
		} catch (EmptyListException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("\n");
	}
	
	/**
	 * Prompts the user for an airport code, creates an AirportStatus 
	 * object for this airport, and adds it after the current node.
	 * If there is no current node, the new node is inserted at the
	 * end of the list.
	 */
	public static void insertAfterCurrent() {
		System.out.print("\nEnter the aiport code: ");
		AirportStatus newAir = new AirportStatus(input.next().toUpperCase());
		newAir.setAllData();
		list.addAfterCurrent(newAir);
		System.out.println("\nInserted:\n");
		newAir.displayTableHeader();
		newAir.displayTableBody();
		System.out.println("\n");
	}
	
	/**
	 * Displays only the data stored in the current node
	 */
	public static void displayAirport() {
		try {
			list.displayCurrentNode();
			System.out.println("\n");
		} catch (NullCursorException e) {
			System.out.println("\n" + e.getMessage() + "\n");
		}
	}
	
	/** 
	 * If there is a current node, it is removed from the list 
	 * and the current node is moved to the next appropriate current node. 
	 * If there is no current node, user is informed of the situation 
	 * with an error message
	 */
	public static void removeNode() {
		String tempAirportCode = "";
		if (!(list.isEmpty())) {
			try {
				tempAirportCode = list.getCursor().getData().getAirportCode();
			} 
			catch (NullCursorException e) {
				System.out.println(e.getMessage());
			}
		}
		if (list.removeCurrentNode() == true) {
			System.out.println("\nRemoved " + tempAirportCode + "\n");
		}
		else {
			System.out.println("\nThis node does not exist\n");
		}
	}
	
	/**
	 * Moves the current node forward by one position if there is another
	 * node in the list. If the current slide is the last in the list,
	 * an appropriate message is displayed to the user and nothing is done
	 */
	public static void moveForward() {
		System.out.println();
		try {
			if (list.moveForward()) {
				System.out.println("The current node has successfully been moved " +
						            "forward one position");
			}
			else {
				System.out.println("There is no next node. Node has not been moved");
			}
		} 
		catch (NullCursorException e) {
			System.out.println(e.getMessage());
		}
		System.out.println();
	}
	
	/**
	 * Moves the current node backward by one position if there is a previous
	 * node in the list. If the current slide is the first in the list,
	 * an appropriate message is displayed to the user and nothing is done
	 */
	public static void moveBackward() {
		System.out.println();
		try {
			if (list.moveBack()) {
				System.out.println("The current node has successfully been moved " +
									"backward one position");
			}
			else {
				System.out.println("There is no previous node. Node has not been moved");
			}
		} catch (NullCursorException e) {
			System.out.println(e.getMessage());
		}
		System.out.println();
	}
	
	/**
	 * Prompts the user for a position number and moves the current node 
	 * to that number node, if it exists. If it does not exist, simply 
	 * an error message is displayed to the user and nothing is done
	 */
	public static void jumpToPosition() {
		int position;
		System.out.print("\nEnter the position: ");
		position = input.nextInt();
		System.out.println();
		if (list.jumpToPosition(position)) {
			System.out.println("The current node has successfully been moved to " +
					"position " + position + "\n");
		}
		else {
			System.out.println("This node does not exist. Node has not been moved\n");
		}
	}
}