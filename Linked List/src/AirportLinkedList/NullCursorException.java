/** 
 * Kevin Young
 * 109561787
 * Homework 2
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 */

public class NullCursorException extends Exception {
	
	/**
	 * This exception is thrown if the cursor is null
	 * @param message the message displayed to the user if the exception is thrown
	 */
	public NullCursorException(String message) {
		super(message);
	}
}
