/** 
 * Kevin Young
 * 109561787
 * Homework 2
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 */

import big.data.DataSource;

/** 
 *  This class contains information about an airports status,
 *  which includes the temperature, visibility, wind speed, and average delay 
 *  The full name and all status data comes from the API response.  
 */
public class AirportStatus {

	/** 
	 * Creates a new AirportStatus object with blank parameters. 
	 * Default constructor
	 * <dt><b>Postcondition:</b><dd>
	 *  this AirportStatus has been initialized to empty values of data
	 */
	public AirportStatus() {}
	
	private DataSource ds; /* this will allow us to access data from the URL */
	private String airportCode; /* 3 letter code representing the airport name
	 								(Ex. JFK = John F Kennedy International) */  
	private String airportName; 
	private double temperature;
	private double visibility; 
	private double windSpeed; 
	private int avgDelay; 
	
	/**
	 * Creates a new AirportStatuc object which takes in the airportCode, and 
	 * sends a request to the API at the URL which pertains to the given airport
	 * @param airportCode the airport code which specifies which airport's data we want
	 * <dt><b>Precondition:</b><dd>
	 *  this airport code exists, and there is access to the internet when the program is run
	 */
	public AirportStatus(String airportCode) {
		ds = DataSource.connect("http://services.faa.gov/airport/status/"
				+ airportCode + "?format=application/xml").load();
		this.airportCode = airportCode;
	}
	
	/**
	 * Changes the airport code of the given airport
	 * @param airportCode the airport code of this airport
	 */
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}
	
	/**
	 * Retrieves and sets the airport name of the given airport by reading data from the API response
	 * <dt><b>Precondition:</b><dd>
	 *  the name field is included in the API response
	 * @throws NullPointerException if the name field is not included in the API response
	 */
	public void setAirportName() {
		try {
			airportName = ds.fetchString("Name");
		} catch (NullPointerException e) {
			airportName = "";
		}
	}
	
	/**
	 * Retrieves and sets the temperature (in fahrenheit) of the given airport by reading data from the API response
	 * <dt><b>Precondition:</b><dd>
	 *  the temperature field is included in the API response
	 * @throws NullPointerException if the temperature field is not included in the API response
	 */
	public void setTemperature() {
		try {
			String temperatureString = ds.fetchString("Weather/Temp");
			String strArr[] = temperatureString.split(" ");
			temperature = Double.parseDouble(strArr[0]);
		} catch (NullPointerException e) {
			temperature = -1;
		}		
	}
	
	/**
	 * Retrieves and sets the visibility of the given airport by reading data from the API response
	 * <dt><b>Precondition:</b><dd>
	 *  the visibility field is included in the API response
	 * @throws NullPointerException if the visibility field is not included in the API response
	 */
	public void setVisibility() {
		try {
			visibility = ds.fetchDouble("Weather/Visibility");
		} catch (NullPointerException e) {
			visibility = -1;
		}
	}
	
	/**
	 * Retrieves and sets the wind speed (in mph) of the given airport by reading data from the API response
	 * <dt><b>Precondition:</b><dd>
	 *  the wind speed field is included in the API response
	 * @throws NullPointerException if the wind speed field is not included in the API response
	 */
	public void setWindSpeed() {
		try {
			String windSpeedString = ds.fetchString("Weather/Wind");
			String strArr[] = windSpeedString.split(" "); 
			windSpeedString = strArr[2].replaceAll("[a-z]","");
			windSpeed = Double.parseDouble(windSpeedString);
		} catch (NullPointerException e) {
			windSpeed = -1;
		}
	}	
	
	/**
	 * Retrieves and sets the average delay of the given airport by reading data from the API response
	 * <dt><b>Precondition:</b><dd>
	 *  there is a delay
	 *  @throws NullPointerException if the delay field is not included in the API response
	 */
	/*  -----------IMPORTANT---------------------
	 * Case 1: If there is an array representing the delay,
	 *   take the entire String which gives the min and max delay of departure and arrival, and get the
	 *   number values (the first represents the min delay of arrival, second represents the max delay of arrive,
	 *   third represents the min delay of departure, and the fourth represents the max delay of departure). 
	 *   Then take the average of arrival and departure, and then average those two values together to get the 
	 *   total average delay.
	 * Case 2: If there is no array representing the delay,
	 *   check if there is a MaxDelay and MinDelay String. If there is,
	 *   then the average of these two numbers is the average delay
	 * Case 3: If there is no array representing the delay,
	 *   and there is no MaxDelay and MinDelay String, then check
	 *   for an AvgDelay String 
	 * If there is no delay, then the average delay is set to -1
	 */
	public void setAvgDelay() {
		if (ds.fetchString("Delay").equals("true")) {	
			/* Check for case 1 */
			try {
				String[] delayStr = ds.fetchStringArray("Status");
				delayStr[0] = delayStr[0].replaceAll("\\D+"," "); /* index 0 is the entire delay, and by replacing
				all non-numbers with spaces, we can get just the numbers */
				int beforeSpacePos = 0; /* represents the position of the space that comes before a number */
				int afterSpacePos = 0; /* represents the position of the space that comes after a number */
				String[] delayStr2 = new String[delayStr[0].length()];
				int count = 0; 
				/* Every time we hit a space, we keep going until we find the 
				 * next space. When we do, this means that a number is located
				 * between this first space and the second space. So we put this
				 * String value into delayStr2, and then we parse the value into an int 
				 */		
				for (int i = 1; i < delayStr[0].length()-1; i++) {
					if (delayStr[0].charAt(i-1) == (' ')) {
						beforeSpacePos = i;
					}
					if (delayStr[0].charAt(i) == (' ')) {
						afterSpacePos = i;
						delayStr2[count] = delayStr[0].substring(beforeSpacePos, afterSpacePos);
						count++; /* increment count when we extract a number */
					}
				}
				/* For reference purposes
				 *  delayStr2[0] = MinDelay of arrival 
				 *  delayStr2[1] = MaxDelay of arrival 
				 *  delayStr2[2] = MinDelay of departure
				 *  delayStr2[3] = MaxDelay of departure
				    System.out.print("MinDelay of arrival: " + delayStr2[0] + "\nMaxDelay of arrival: " + delayStr2[1]
				    + "\nMinDelay of departure: " + delayStr2[2] + "\nMaxDelay of departure: " + delayStr2[3]);
				 */
				avgDelay = (int)(Math.ceil((((Integer.parseInt(delayStr2[0]) + Integer.parseInt(delayStr2[2])) / 2) + 
						((Integer.parseInt(delayStr2[2]) + Integer.parseInt(delayStr2[3])) / 2)) / 2));					
				}
				catch (NullPointerException e) {
					/* Check for case 2 */
					try {
						String maxDelayStr = ds.fetchString("Status/MaxDelay").replaceAll("\\D+", "");
						int maxDelayInt = Integer.parseInt(maxDelayStr); 
						String minDelayStr = ds.fetchString("Status/MinDelay").replaceAll("\\D+", ""); 
						int minDelayInt = Integer.parseInt(minDelayStr);
						avgDelay = (maxDelayInt + minDelayInt) / 2 ;
					}
					catch (NullPointerException e1) {
						/* Check for case 3 */
						try {
							String avgDelayStr = ds.fetchString("Status/AvgDelay");
							String[] avgDelayArr = avgDelayStr.split(" ");
							avgDelay = Integer.parseInt(avgDelayArr[0]);	
						}
						/* If all cases fail, average delay is -1 */
						catch (NullPointerException e2) {
							avgDelay = -1;
						}
					}					
				}
		}
		else {
			avgDelay = -1;
		}
	}
	
	/**
	 * Set all the data values for this airplane
	 */
	public void setAllData() {
		setAvgDelay();
		setWindSpeed();
		setVisibility();
		setTemperature();
		setAirportName();
	}
	
	/**
	 * Returns the airport code in all upper-case
	 * @return the airport code of this airport
	 */
	public String getAirportCode() {
		return airportCode.toUpperCase();
	}
	
	/**
	 * Returns the airport name 
	 * @return the airport name of this airport 
	 */
	public String getAirportName() {
		return airportName;
	}
	
	/**
	 * Returns the temperature 
	 * @return the temperature of this airport
	 */
	public double getTemperature() {
		return temperature;
	}
	
	/**
	 * Returns the visibility 
	 * @return the visibility of this airport
	 */
	public double getVisibility() {
		return visibility;
	}
	
	/**
	 * Returns the wind speed
	 * @return the wind speed of this airport
	 */
	public double getWindSpeed() {
		return windSpeed;
	}
	
	/**
	 * Returns the average delay
	 * @return the average delay of this airport
	 */
	public int getAvgDelay() {
		return avgDelay;
	}
	
	/**
	 *  Set up the table which will display all of the airport's data
	 */
	public void displayTableHeader() {
		System.out.printf("%-9s %-45s %-16s %-12s %-16s %14s" ,
				"Symbol" , "Name" , "Temperature(F)" , "Visibility" ,
				"Wind Speed(mph)" , "Average Delay(min)");
		System.out.println("\n---------------------------------------" +
				"----------------------------------------------------" +
				"------------------------------");
	}
	
	/**
	 * Meant to be attached to the header, this contains all the data of the airport. The purpose of
	 * this is to be able to only call the header once while calling the body for each airport
	 */
	public void  displayTableBody() {
		if (this == null) {
			System.out.print("null");
		}
		else {
			System.out.printf("%-9s %-45s %-16.2f %-12.2f %-16.2f %-10d" , 
					airportCode , airportName ,
					temperature , visibility , 
					windSpeed , avgDelay);
		}
	}		
	
	/**
	 * Return the table header as a string. This will be used for the GUI
	 * <dt><b>Precondition:</b><dd>
	 *  this airport has been instantiated
	 */
	public String tableHeaderToString() {
		if (this == null)
			return "";
		else {
			return String.format("%-9s %-45s %-16s %-12s %-16s %14s" ,
				"Symbol" , "Name" , "Temperature(F)" , "Visibility" ,
				"Wind Speed(mph)" , "Average Delay(min)") + 
				"\n---------------------------------------" +
				"----------------------------------------------------"
				+ "------------------------------"; 
			
		}
	}
	
	/**
	 * Return the table body as a string. This will be used for the GUI
	 * <dt><b>Precondition:</b><dd>
	 *  this airport has been instantiated
	 */
	public String tableBodyToString() {
		if (this == null)
			return "";
		else {		
			return String.format("%-9s %-45s %-16.2f %-12.2f %-16.2f %-10d" , 
					airportCode , airportName ,
					temperature , visibility , 
					windSpeed , avgDelay);
		}
	}
	
	/** @Override
	 * Return this instance as a String. This will be used for the GUI
	 */
	public String toString() {
		return tableHeaderToString() + "\n" + tableBodyToString();
	}
}