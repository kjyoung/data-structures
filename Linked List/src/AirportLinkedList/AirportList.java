/** 
 * Kevin Young
 * 109561787
 * Homework 2
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 */

/**
 * This class contains references to the head and tail of a list of AirportNode nodes, 
 * as well as a cursor representing the current AirportNode node *
 */
public class AirportList {

	private AirportNode head; /* the node at the very beginning of the list */
	private AirportNode cursor; /* the current AirportNode node */
	private AirportNode tail; /* the node at the very end of the list */
	
	/**
	 * Create an instance of AirportList with a null head, cursor, and tail
	 */
	public AirportList() {
		head = null;
		cursor = null;
		tail = null;
	}
	
	/**
	 * Inserts the new data at the end of the AirportList. The current node 
	 * is now be the newly created node. If the list is empty, a new node is created
	 * which will now be the head, tail, and cursor
	 * @param newData
	 */
	public void addToEnd(AirportStatus newData) {
		AirportNode newNode = new AirportNode(newData);
		if (isEmpty()) {
			head = newNode;
			tail = head;
		}
		else {
			cursor = tail;
			newNode.setPrev(cursor);
			cursor.setNext(newNode);
			tail = newNode;
		}
		cursor = newNode;
	}
	
	/**
	 * Inserts the new data into the AirportList such that the
	 * new node directly follows the current node, if it exists.
	 * If there is no current node (i.e., the current node is null),
	 * insert the node at the end of the list. The current
	 * node will now be the newly created node. 
	 * @param newData the airport to be added
	 */
	public void addAfterCurrent(AirportStatus newData) {
		if (cursor == null) {
			addToEnd(newData);
		}
		else if (cursor.getNext() == null) {
			AirportNode newNode = new AirportNode(newData);
			cursor.setNext(newNode);
			newNode.setPrev(cursor);
			cursor = cursor.getNext();
			tail = cursor;
			cursor = newNode;
		}
		else {
			AirportNode newNode = new AirportNode(newData);
			cursor.getNext().setPrev(newNode);
			newNode.setNext(cursor.getNext());
			cursor.setNext(newNode);
			newNode.setPrev(cursor);
			cursor = newNode;
		}			
	}	
	
	/**
	 * Returns true if the list is empty
	 * @return if the list is empty
	 */
	public boolean isEmpty() {
		return (head == null);
	}
	
	/**
	 * Display the data for each airport in the list
	 * <dt><b>Precondition:</b><dd>
	 *  the list is not empty
	 * @throws EmptyListException if the list is empty
	 */
	public void displayCurrentList() throws EmptyListException {
		/* For testing 
		cursor = head;
		int i = 0;
		while (cursor != null) {
			System.out.print(i + ": " + cursor.getData().getAirportCode() + " ");
			i++;
			cursor = cursor.getNext();
		}
		 */
		if (isEmpty() == true) {
			throw new EmptyListException("There is nothing to display, the list is empty");
		}
		else {
			AirportNode tempCursor = cursor;
			cursor = head;
			cursor.displayTableHeader();
			while(cursor != null) {
				cursor.displayTableBody();
				cursor = cursor.getNext();
				System.out.println();
			}
			cursor = tempCursor;
		}
	}	
	
	/** @Override
	 * Return the list as a String, which will be used for the GUI
	 */
	public String toString() {
		/* For testing 
		cursor = head;
		int i = 0;
		while (cursor != null) {
			System.out.print(i + ": " + cursor.getData().getAirportCode() + " ");
			i++;
			cursor = cursor.getNext();
		}
		 */
		if (isEmpty() == true) {
			return "There is nothing to display, the list is empty";
		}
		else {
			AirportNode tempCursor = cursor;
			cursor = head;
			String listToString = cursor.getData().tableHeaderToString() + "\n";
			while(cursor != null) {
				listToString += cursor.getData().tableBodyToString();
				cursor = cursor.getNext();
				listToString += "\n";
			}
			cursor = tempCursor;
			return listToString;
		}
	}	
	
	/**
	 * Display just the current node
	 * <dt><b>Precondition:</b><dd>
	 *  the current node is not null
	 * @throws NullCursorException if the node is null
	 */
	public void displayCurrentNode() throws NullCursorException {
		if (cursor == null)
			throw new NullCursorException("This node does not exist");
		else {
			System.out.println("\nCurrent node: \n");
			cursor.displayTableHeader();
			cursor.displayTableBody();
		}
	}

	/** 
	 * Removes the current node from the list
	 * <dt><b>Precondition:</b><dd>
	 *  the node exists
	 * @return true if the node was successfully removed
	 */
	public boolean removeCurrentNode() {
		if (cursor == null) {
			return false;
		}
		else {
			/* if the current node is the tail, the current node 
			 *  becomes the node before the one that was just removed */			 
			if (cursor.getNext() == null && cursor.getPrev() != null) {
				AirportNode tempCursor = cursor.getPrev();
				cursor.getPrev().setNext(null);				
				cursor = tempCursor;
				tail = cursor;
			}
			/* if the current node is the only node in the list, the current node 
			 *  becomes null */
			else if (cursor.getNext() == null && cursor.getPrev() == null) {
				head = null;
				tail = null;
				cursor = null;				
			}
			/* the current node is now the node after the one that was just removed */
			else {
				AirportNode tempCursor = cursor.getNext();
				if (cursor == head) {
					cursor.getNext().setPrev(null);
					head = tempCursor;
					cursor = tempCursor;
				}
				else {
					cursor.getNext().setPrev(cursor.getPrev());
					cursor.getPrev().setNext(cursor.getNext());
					cursor = tempCursor;
				}
			}
			return true;
		}
	}
	
	/**
	 * Returns the current node
	 * @return the current node
	 * @throws NullCursorException if the current node doesn't exist
	 */
	public AirportNode getCursor() throws NullCursorException {
		if (cursor == null) {
			throw new NullCursorException("The current node does not exist");
		}
		else {
			return cursor;
		}
	}
		
	/**
	 * The current node is moved forward in the list by one position if 
	 * a node exists after the current one and returns true. If there
	 * is no next node, the current node remains the same and 
	 * return false. 
	 * <dt><b>Precondition:</b><dd>
	 *  there is a next node, and the curent node exists
	 * @return true if the node was successfully moved forward
	 * @throws NullCursorException if the node doesn't exist
	 */
	public boolean moveForward() throws NullCursorException {
		if (cursor == null) {
			throw new NullCursorException("There is no current node");
		}
		else if (cursor.getNext() == null) {
			return false;
		}
		else {
			cursor = cursor.getNext();
			return true;
		}			
	}
	
	/**
	 * Moves the current node backwards in the list by one position
	 * if a node exists before the current one and returns true. 
	 * If there is no previous node, the current node remains the
	 * same and returns false.
	 * <dt><b>Precondition:</b><dd>
	 * this node exists, and a node exists before the current one 
	 * @return true if the node was successfully moved back
	 * @throws NullCursorException if the node doesn't exist
	 */
	public boolean moveBack() throws NullCursorException {
		if (cursor == null) {
			throw new NullCursorException("There is no current node");
		}
		else if (cursor.getPrev() == null) {
			return false;
		}
		else {
			cursor = cursor.getPrev();
			return true;
		}
	}		
	
	/**
	 * Moves the current node to the given position in the AirportList.
	 * The first airport in the AirportList is position 1. If the given
	 * position doesn't exist in the list, the current node is left where
	 * it was and false is returned.
	 * <dt><b>Precondition:</b><dd>
	 * the given position exists 
	 * @param position the given position to jump to
	 * @return true if the current node was successfully changed
	 */
	public boolean jumpToPosition(int position) {
		int count = 1;
		AirportNode tempCursor = cursor;
		if (isEmpty()) {
			return false;
		}
		cursor = head;
		while (cursor != null) {
			if (count == position) {
					return true;
				}
			else {
				count++;
				cursor = cursor.getNext();
			}
		}
		cursor = tempCursor;
		return false;				
	}
}