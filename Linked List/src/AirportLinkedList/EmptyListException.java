/** 
 * Kevin Young
 * 109561787
 * Homework 2
 * Recitation 05
 * TA Name: Sun Lin
 * Grading TA: Pengyue Zhang 
 * @author Kevin Young
 */

public class EmptyListException extends Exception {
	
	/** 
	 * This exception is thrown if the linkedlist is empty
	 * @param message the message to display if this exception is thrown
	 */
	public EmptyListException(String message) {
		super(message);
	}
}
